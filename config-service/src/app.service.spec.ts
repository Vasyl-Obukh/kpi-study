import { AppService } from './app.service';
import { configData } from './data';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(() => {
    appService = new AppService();
  });

  describe('getConfigData', () => {
    describe('Successful cases', () => {
      it('should return config data', () => {
        expect(appService.getConfigData()).toEqual(configData);
      });
    });
  });
});
