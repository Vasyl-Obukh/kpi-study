import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { ConfigData } from './data';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('data')
  @ApiTags('Config data')
  @ApiOkResponse({ description: 'Config data has been successfully sent.' })
  getConfigData(): ConfigData {
    return this.appService.getConfigData();
  }
}
