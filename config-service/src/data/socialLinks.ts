interface SocialLink {
  name: string;
  url: string;
}

export const socialLinks: SocialLink[] = [
  {
    name: 'youtube',
    url: 'https://www.youtube.com/channel/UCqD3lg-4bSVCKFhXBRJUFmg',
  },
  {
    name: 'facebook',
    url: 'https://www.facebook.com/vasia.obyh',
  },
  {
    name: 'twitter',
    url: 'https://twitter.com/ObukhVasia',
  },
  {
    name: 'instagram',
    url: 'https://www.instagram.com/vasia_obukh',
  },
  {
    name: 'linkedIn',
    url: 'https://www.linkedin.com/in/vasyl-obukh-682ba7188/',
  },
];
