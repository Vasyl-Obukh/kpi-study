import { socialLinks } from './socialLinks';
import { textLabels } from './textLabels';

export interface ConfigData {
  socialLinks?: typeof socialLinks;
  textLabels?: typeof textLabels;
}

export const configData: ConfigData = {
  socialLinks,
  textLabels,
};
