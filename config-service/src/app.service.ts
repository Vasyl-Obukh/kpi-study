import { Injectable } from '@nestjs/common';
import { configData, ConfigData } from './data';

@Injectable()
export class AppService {
  getConfigData(): ConfigData {
    return configData;
  }
}
