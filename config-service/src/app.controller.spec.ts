import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

const appServiceMock: AppService = {
  getConfigData: () => ({
    textLabels: {
      heading: 'Sample heading',
    },
  }),
};

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AppService)
      .useValue(appServiceMock)
      .compile();

    appController = app.get<AppController>(AppController);
  });

  describe('getConfigData()', () => {
    describe('Successful cases', () => {
      it('should return config data', () => {
        expect(appController.getConfigData()).toEqual({
          textLabels: {
            heading: 'Sample heading',
          },
        });
      });
    });
  });
});
