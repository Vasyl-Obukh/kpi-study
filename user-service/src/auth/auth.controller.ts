import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common';

import { LocalAuthGuard } from './local.guard';
import { AccessToken, AuthService } from './auth.service';
import { UserDto } from '../user/user.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  async register(@Body() userDto: UserDto): Promise<AccessToken> {
    return await this.authService.register(userDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req): Promise<AccessToken> {
    return this.authService.getToken(req.user);
  }
}
