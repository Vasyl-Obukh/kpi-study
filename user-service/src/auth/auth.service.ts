import { ConflictException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UserService } from '../user/user.service';
import { User, UserWithSafeFields } from '../user/user.schema';
import { UserDto } from '../user/user.dto';
import { ErrorMessages } from '../constants/error-messages';

export interface AccessToken {
  ACCESS_TOKEN: string;
}

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<UserWithSafeFields | null> {
    const user: User = await this.userService.findUserByEmail(email);
    if (user) {
      const isPasswordMatch = await bcrypt.compare(password, user.password);
      if (isPasswordMatch) {
        return UserService.getUserSafeFields(user);
      }
    }

    return null;
  }

  async register(userDto: UserDto): Promise<AccessToken> | never {
    const isUserExist = !!(await this.userService.findUserByEmail(
      userDto.email,
    ));
    if (isUserExist) {
      throw new ConflictException(ErrorMessages.ACCOUNT_EXIST_CONFLICT);
    }

    const user: User = await this.userService.createUser(userDto);
    return this.getToken(user);
  }

  async getToken({ _id }: UserWithSafeFields): Promise<AccessToken> {
    return {
      ACCESS_TOKEN: this.jwtService.sign({ sub: _id }),
    };
  }

  validateToken(jwt: string) {
    return this.jwtService.verify(jwt);
  }
}
