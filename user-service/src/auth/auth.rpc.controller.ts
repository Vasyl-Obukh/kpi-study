import { Controller, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';

@Controller()
export class AuthRpcController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @MessagePattern({ cmd: 'validate-jwt' })
  async validateToken(token: string) {
    try {
      const { sub: userId } = this.authService.validateToken(token);
      return this.userService.findUserById(userId);
    } catch (error) {
      Logger.error(error);
      return false;
    }
  }
}
