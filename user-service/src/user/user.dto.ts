import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

import { ErrorMessages } from '../constants/error-messages';

export class UserDto {
  @IsNotEmpty({ message: ErrorMessages.EMPTY_NAME })
  @IsString({ message: ErrorMessages.NAME_TYPE })
  name: string;

  @IsNotEmpty({ message: ErrorMessages.EMPTY_EMAIL })
  @IsString({ message: ErrorMessages.EMAIL_TYPE })
  @IsEmail({}, { message: ErrorMessages.EMAIL_INVALID })
  email: string;

  @IsNotEmpty({ message: ErrorMessages.EMPTY_PASSWORD })
  @IsString({ message: ErrorMessages.PASSWORD_TYPE })
  @MinLength(8, { message: ErrorMessages.PASSWORD_INVALID_LENGTH })
  password: string;
}
