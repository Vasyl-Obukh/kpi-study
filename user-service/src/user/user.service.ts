import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { User, UserDocument, UserWithSafeFields } from './user.schema';
import { Model } from 'mongoose';
import { UserDto } from './user.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  static getUserSafeFields({ _id, name, email }: User): UserWithSafeFields {
    return { _id, name, email };
  }

  async createUser(userDto: UserDto): Promise<User> {
    return new this.userModel(userDto).save();
  }

  async findUserByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email }).exec();
  }

  async findUserById(id: string): Promise<UserWithSafeFields> {
    return UserService.getUserSafeFields(
      await this.userModel.findById(id).exec(),
    );
  }

  async findUsersByIds(ids: string[]): Promise<UserWithSafeFields[]> {
    const users: User[] = await Promise.all(
      ids.map(id => this.userModel.findById(id).exec()),
    );

    return users.map(user => UserService
      .getUserSafeFields(user),
    );
  }
}
