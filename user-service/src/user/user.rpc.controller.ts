import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { UserService } from './user.service';
import { UserWithSafeFields } from './user.schema';

@Controller()
export class UserRpcController {
  constructor(
    private readonly userService: UserService,
  ) {}

  @MessagePattern({ cmd: 'find-users-by-ids' })
  findUsersByIds(ids: string[]): Promise<UserWithSafeFields[]> {
    return this.userService.findUsersByIds(ids);
  }
}
