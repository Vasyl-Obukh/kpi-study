export enum ErrorMessages {
  ACCOUNT_EXIST_CONFLICT = 'error-account-exist-conflict',
  EMPTY_NAME = 'error-empty-name',
  NAME_TYPE = 'error-name-type',
  EMPTY_EMAIL = 'error-empty-email',
  EMAIL_TYPE = 'error-email-type',
  EMAIL_INVALID = 'error-email-invalid',
  EMPTY_PASSWORD = 'error-empty-password',
  PASSWORD_TYPE = 'error-password-type',
  PASSWORD_INVALID_LENGTH = 'error-password-invalid-length',
}
