import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { UnauthorizedExceptionFilter } from './filters/unauthotized-exception.filter';
import { DtoMismatchValidationPipe } from './pipe/dto-mismatch.pipe';

// tslint:disable-next-line:radix
const PORT = parseInt(process.env.PORT) || 4043;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.TCP,
    options: {
      // host: '0.0.0.0',
      port: PORT,
    },
  }, {
    inheritAppConfig: true,
  });

  app.useGlobalPipes(DtoMismatchValidationPipe);
  app.useGlobalFilters(new UnauthorizedExceptionFilter());

  const options = new DocumentBuilder()
    .setTitle('User Service API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.startAllMicroservicesAsync();
  await app.listen(PORT);
}

bootstrap();
