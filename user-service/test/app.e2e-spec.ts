import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { AppService } from '../src/app.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const appServiceMock: AppService = {
    getConfigData: () => ({
      textLabels: {
        heading: 'Sample heading',
      },
    }),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AppService)
      .useValue(appServiceMock)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/data', () => {
    describe('Successful cases', () => {
      it('should return config data', done => {
        return request(app.getHttpServer())
          .get('/data')
          .expect(200)
          .expect({
            textLabels: {
              heading: 'Sample heading',
            },
          })
          .end(() => done());
      });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
