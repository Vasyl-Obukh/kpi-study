export enum ErrorMessages {
  EMPTY_TITLE = 'error-empty-title',
  TITLE_TYPE = 'error-title-type',
  EMPTY_IMAGE = 'error-empty-image',
  IMAGE_TYPE = 'error-image-type',
  EMPTY_PRIVACY = 'error-empty-privacy',
  PRIVACY_TYPE = 'error-privacy-type',
}
