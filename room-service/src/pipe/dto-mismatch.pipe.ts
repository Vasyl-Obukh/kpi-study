import {
  BadRequestException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';

export const DtoMismatchValidationPipe = new ValidationPipe({
  exceptionFactory: (errors: ValidationError[]): BadRequestException => {
    return new BadRequestException(
      errors.map((error) => Object.values(error.constraints)).flat(),
    );
  },
});
