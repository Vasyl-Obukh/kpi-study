import { Inject, UseGuards } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Server, Socket } from 'socket.io';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { v4 as generateId } from 'uuid';

import { JwtWSAuthGuard } from './guards/jwt.ws.guard';
import { RoomService } from './room/room.service';
import { Room } from './room/room.schema';
import { User } from './interfaces/user.interface';

interface Peer extends User {
  peerId: string;
}

type Peers = Record<string, Peer>;

type Rooms = Record<string, Peers>;

@WebSocketGateway()
export class AppGateway implements OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  peers: Peers = {};
  rooms: Rooms = {};

  constructor(
    @Inject('USER_SERVICE')
    private readonly userClient: ClientProxy,
    private readonly roomService: RoomService,
  ) {}

  getRoomDataByPeerId(id: string) {
    return Object.entries(this.rooms).find((room) => id in room[1]) || [];
  }

  handleDisconnect(client) {
    const [roomId, peers] = this.getRoomDataByPeerId(client.id);
    if (!peers) return;
    delete peers[client.id];
    client.in(roomId).emit('disconnectPeer', client.id);
  }

  @UseGuards(JwtWSAuthGuard)
  @SubscribeMessage('enter-room')
  async handleRoomEntering(
    @MessageBody() roomId: string,
    @ConnectedSocket() client: Socket,
  ) {
    const room: Room = await this.roomService.getRoomById(roomId);
    if (!room) throw new WsException('room was not found');

    client.join(room._id);
    if (!this.rooms[room._id]) {
      this.rooms[room._id] = {};
    }

    if (!this.rooms[roomId][client.id]) {
      this.rooms[roomId][client.id] = {
        ...client.user,
        peerId: client.id,
      };
    }

    client.emit(
      'sendPeerList',
      this.getPeersWithoutTargetClient(this.rooms[roomId], client),
    );
  }

  @UseGuards(JwtWSAuthGuard)
  @SubscribeMessage('signal')
  handleSignal(
    @MessageBody() data: { to: string; signal: string },
    @ConnectedSocket() client: Socket,
  ) {
    this.server.to(data.to).emit('signal', {
      signal: data.signal,
      callerId: client.id,
    });
  }

  @UseGuards(JwtWSAuthGuard)
  @SubscribeMessage('call')
  handleCall(@MessageBody() callee: string, @ConnectedSocket() client: Socket) {
    const [roomId] = this.getRoomDataByPeerId(client.id);
    this.server.to(callee).emit('call', this.rooms[roomId][client.id]);
  }

  @UseGuards(JwtWSAuthGuard)
  @SubscribeMessage('sendMessage')
  handleIncomingMessage(
    @MessageBody() message: string,
    @ConnectedSocket() client: Socket,
  ) {
    const [roomId] = this.getRoomDataByPeerId(client.id);
    this.server.in(roomId).emit('sendMessage', {
      id: generateId(),
      text: message,
      date: new Date(),
      author: client.user,
    });
  }

  getPeersWithoutTargetClient(peers: Peers, client: Socket): Peers {
    return Object.fromEntries(
      Object.entries(peers).filter(([id]) => id !== client.id),
    );
  }
}
