import { Global, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MongooseModule } from '@nestjs/mongoose';

import { AppGateway } from './app.gateway';
import { RoomModule } from './room/room.module';
import { CloudinaryModule } from './cloudinary/cloudinary.module';

const USER_SERVICE_HOST = process.env.USER_SERVICE_HOST || 'localhost';
const USER_SERVICE_PORT = parseInt(process.env.USER_SERVICE_PORT);

@Global()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'USER_SERVICE',
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        transport: Transport.TCP,
        options: {
          host: USER_SERVICE_HOST,
          port: USER_SERVICE_PORT,
        },
      },
    ]),
    MongooseModule.forRoot(
      process.env.MONGODB_URI || 'mongodb://localhost/kpi-study',
      {
        useCreateIndex: true,
      },
    ),
    RoomModule,
    CloudinaryModule,
  ],
  providers: [AppGateway],
  exports: [ClientsModule],
})
export class AppModule {}
