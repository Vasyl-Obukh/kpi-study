import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { User } from '../interfaces/user.interface';

@Schema()
export class Room {
  // tslint:disable-next-line:variable-name
  _id: string;

  @Prop()
  title: string;

  @Prop()
  image: string;

  @Prop()
  private: boolean;

  @Prop()
  ownerId: string;
}

export type RoomDocument = Room & Document;
export type PopulatedRoom = Omit<Room, 'ownerId'> & {
  owner: User;
};

export const RoomSchema = SchemaFactory.createForClass(Room);
