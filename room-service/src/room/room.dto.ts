import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

import { ErrorMessages } from '../constants/error-messages';

export class RoomDto {
  @IsNotEmpty({ message: ErrorMessages.EMPTY_TITLE })
  @IsString({ message: ErrorMessages.TITLE_TYPE })
  title: string;

  @IsNotEmpty({ message: ErrorMessages.EMPTY_IMAGE })
  @IsString({ message: ErrorMessages.IMAGE_TYPE })
  image: string;

  @IsNotEmpty({ message: ErrorMessages.EMPTY_PRIVACY })
  @IsBoolean({ message: ErrorMessages.PRIVACY_TYPE })
  private: boolean;
}
