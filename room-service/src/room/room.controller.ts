import {
  Body,
  Controller,
  Get,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';

import { RoomDto } from './room.dto';
import { JwtAuthGuard } from '../guards/jwt.guard';
import { UserData } from '../decorators/user-data.decorator';
import { User } from '../interfaces/user.interface';
import { RoomService } from './room.service';
import { PopulatedRoom } from './room.schema';

@Controller('data')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async createRoom(
    @Body() roomDto: RoomDto,
    @UserData() user: User,
  ): Promise<void> {
    await this.roomService.createRoom({
      ...roomDto,
      ownerId: user._id,
    });
  }

  @Get()
  async getPublicRooms(
    @Query('amount', ParseIntPipe) amount = 0,
  ): Promise<PopulatedRoom[]> {
    return this.roomService.getPublicPopulatedRooms(amount);
  }
}
