import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { RoomDto } from './room.dto';
import { PopulatedRoom, Room, RoomDocument } from './room.schema';
import { CloudinaryService } from '../cloudinary/cloudinary.service';
import { User } from '../interfaces/user.interface';

type RoomCreationData = RoomDto & {
  ownerId: string;
};

@Injectable()
export class RoomService {
  constructor(
    @Inject('USER_SERVICE')
    private readonly client: ClientProxy,
    @InjectModel(Room.name)
    private readonly roomModel: Model<RoomDocument>,
    private readonly cloudinaryService: CloudinaryService,
  ) {}

  private static getUniqueOwnerIds(rooms: Room[]): string[] {
    return [...new Set(rooms.map((room) => room.ownerId))];
  }

  private static mergeOwnersIntoRooms(
    rooms: Room[],
    users: User[],
  ): PopulatedRoom[] {
    return rooms.map(({ ownerId, ...room }) => ({
      ...room,
      owner: users.find(({ _id }) => _id === ownerId),
    }));
  }

  async createRoom(roomCreationData: RoomCreationData): Promise<Room> {
    const { secure_url: image } = await this.cloudinaryService.uploadImage(
      roomCreationData.image,
    );

    return new this.roomModel({ ...roomCreationData, image }).save();
  }

  async getPublicRooms(amount = 0): Promise<Room[]> {
    const rooms: RoomDocument[] = await this.roomModel
      .find({ private: false }, null, {
        limit: amount,
      })
      .exec();

    return rooms.map((room) => room.toObject());
  }

  async getPublicPopulatedRooms(amount: number): Promise<PopulatedRoom[]> {
    const rooms: Room[] = await this.getPublicRooms(amount);
    const users: User[] = await this.client
      .send({ cmd: 'find-users-by-ids' }, RoomService.getUniqueOwnerIds(rooms))
      .toPromise();

    return RoomService.mergeOwnersIntoRooms(rooms, users);
  }

  async getRoomById(roomId: string): Promise<Room> {
    if (!Types.ObjectId.isValid(roomId)) return null;
    return this.roomModel.findById(roomId).exec();
  }
}
