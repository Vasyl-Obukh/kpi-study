import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { urlencoded, json } from 'express';

import { AppModule } from './app.module';
import { DtoMismatchValidationPipe } from './pipe/dto-mismatch.pipe';
import { UnauthorizedExceptionFilter } from './filters/unauthotized-exception.filter';

const PORT = parseInt(process.env.PORT) || 4042;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.TCP,
    options: {
      port: PORT,
    },
  });

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.useGlobalFilters(new UnauthorizedExceptionFilter());
  app.useGlobalPipes(DtoMismatchValidationPipe);

  await app.startAllMicroservicesAsync();
  await app.listen(PORT);
}

bootstrap();
