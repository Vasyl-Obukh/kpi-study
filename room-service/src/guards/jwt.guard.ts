import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

import { User } from '../interfaces/user.interface';

type AuthorizedRequest = Request & {
  user: User;
};

const extractJwtFromRequest = (req: Request): string =>
  req.headers['authorization'].split(' ')[1];

@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(
    @Inject('USER_SERVICE')
    private readonly client: ClientProxy,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: AuthorizedRequest = context.switchToHttp().getRequest();
    const jwt = extractJwtFromRequest(request);

    const response = await this.client
      .send({ cmd: 'validate-jwt' }, jwt)
      .toPromise();

    if (response) {
      request.user = response;
      return true;
    }

    throw new UnauthorizedException();
  }
}
