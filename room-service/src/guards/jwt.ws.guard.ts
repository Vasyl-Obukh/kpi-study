import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Socket } from 'socket.io';
import { ClientProxy } from '@nestjs/microservices';
import { WsException } from '@nestjs/websockets';

import { User } from '../interfaces/user.interface';

@Injectable()
export class JwtWSAuthGuard implements CanActivate {
  constructor(
    @Inject('USER_SERVICE')
    private readonly userClient: ClientProxy,
  ) {}

  static extractJwtFromClient(client: Socket): string {
    return client.handshake.headers.authorization.split(' ')[1];
  }

  static getUserData(client: Socket, userClient: ClientProxy): Promise<User> {
    const jwt = JwtWSAuthGuard.extractJwtFromClient(client);

    return userClient.send({ cmd: 'validate-jwt' }, jwt).toPromise();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client: Socket = context.switchToWs().getClient();
    const user: User = await JwtWSAuthGuard.getUserData(
      client,
      this.userClient,
    );

    if (user) {
      client.user = user;
      return true;
    }

    throw new WsException('Unauthorized');
  }
}
