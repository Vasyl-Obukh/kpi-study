#!/bin/sh
unset RUN_DOCKERIZED

while getopts 'd' opt
do
  case $opt in
    d) RUN_DOCKERIZED=true;;
    *) echo "Error: Invalid flag";;
  esac
done

if [ "$RUN_DOCKERIZED" = true ]
then
  docker-compose up
else
  . ./.env
  export IP_ADDRESS=$(ipconfig getifaddr en0)
  export API_ROOT_URL=http://localhost
  export WS_URL=http://localhost:$ROOM_SERVICE_PORT
  export JWT_SECRET=$JWT_SECRET
  export CLOUDINARY_CLOUD_NAME=$CLOUDINARY_CLOUD_NAME
  export CLOUDINARY_API_KEY=$CLOUDINARY_API_KEY
  export CLOUDINARY_API_SECRET=$CLOUDINARY_API_SECRET
  export MONGODB_URI=mongodb://localhost:$MONGO_DB_PORT/$DB_NAME
  export FAST_REFRESH=false
  export USER_SERVICE_PORT=$USER_SERVICE_PORT

  export PORT=$CONFIG_SERVICE_PORT && cd ./config-service && npm run start:dev &
  export PORT=$ROOM_SERVICE_PORT && cd ../room-service && npm run start:dev &
  export PORT=$USER_SERVICE_PORT && cd ../user-service && npm run start:dev &
  export PORT=$CLIENT_PORT && cd ../client && npm run start &

  cd .. && docker-compose -f docker-compose-nginx.yml up
fi
