import axios, { AxiosRequestConfig } from 'axios';

export const ACCESS_TOKEN = 'ACCESS_TOKEN';

const getAccessToken = () => localStorage.getItem(ACCESS_TOKEN);

export const getAuthorizationHeader = (): { Authorization: string } => {
  const accessToken = getAccessToken();

  return {
    Authorization: `Bearer ${accessToken}`,
  };
};

axios.interceptors.request.use((config) => {
  Object.assign(config.headers,
    getAuthorizationHeader(),
  );

  return config;
});

axios.interceptors.response.use(
  (response) => response.data,
  (error) => Promise.reject(error.response.data)
);

export const HttpClient = {
  get<T>(url: string, options?: AxiosRequestConfig): Promise<T> {
    return axios.get(url, options);
  },
  post<T>(url: string, data: object): Promise<T> {
    return axios.post(url, data);
  },
};
