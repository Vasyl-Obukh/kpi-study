import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { InputField } from 'components/forms/elements/InputField';
import { ImageUploadField } from 'components/forms/elements/ImageUploadField';
import { Checkbox } from 'components/forms/elements/Checkbox';
import { Button } from 'components/styled/Button';
import { selectTextLabels } from 'store/config/selectors';
import { RoomFormData } from 'store/rooms/types';

import * as Styled from './styles';

const initialValues: RoomFormData = {
  title: '',
  image: '',
  private: false,
};

interface RoomFormProps {
  onSubmit: (values: RoomFormData) => void;
  submitButtonLabel?: string;
}

export const RoomForm: React.FC<RoomFormProps> = ({ onSubmit, submitButtonLabel }) => {
  const textLabels = useSelector(selectTextLabels);

  const validationSchema = useMemo(() => Yup.object({
    title: Yup.string()
      .required(textLabels['room-form-title-required-label']),
    image: Yup.string(),
    private: Yup.boolean(),
  }), [textLabels]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Styled.Form>
        <InputField
          name='title'
          placeholder={textLabels['room-form-title-label']}
        />
        <ImageUploadField
          name='image'
        />
        <Checkbox name='private'>
          {textLabels['room-form-privacy-checkbox-label']}
        </Checkbox>
        <Button type='submit'>{submitButtonLabel}</Button>
      </Styled.Form>
    </Formik>
  );
};
