import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { DragAndDrop } from 'components/forms/elements/DragAndDrop';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';
import { selectTextLabels } from 'store/config/selectors';
import { useField } from 'formik';

interface ImageDropProps {
  name: string;
}

const VALID_IMAGE_TYPES = ['image/gif', 'image/jpeg', 'image/png', 'image/svg+xml'];

const findValidImageFile = (files: FileList): File | undefined => Array.from(files)
  .find((file) => VALID_IMAGE_TYPES.includes(file.type));

export const ImageDrop: React.FC<ImageDropProps> = ({
  children,
  name,
}) => {
  const dispatch = useDispatch();
  const [field, meta, helpers] = useField(name);
  const textLabels = useSelector(selectTextLabels);

  // const dispatchIncompatibilityError = useCallback(() => {
  //   dispatch(globalActions.addMessage({
  //     type: MessageType.Error,
  //     text: textLabels['error-incompatible-image-drop'],
  //   }));
  // }, [dispatch, textLabels]);

  const handleImageDrop = useCallback((files: FileList): void => {
    // const imageFile = findValidImageFile(files);
    //
    // if (imageFile) {
    //   onImageDrop(imageFile);
    // } else {
    //   dispatchIncompatibilityError();
    // }
  }, []);

  return (
    <DragAndDrop onFilesDrop={handleImageDrop}>
      {children}
    </DragAndDrop>
  );
};
