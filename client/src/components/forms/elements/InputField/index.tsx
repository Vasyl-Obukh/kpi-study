import React, { useState } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';

import { ErrorMessage } from 'components/forms/elements/ErrorMessage';

import * as Styled from './styles';

interface InputFieldProps {
  name: string;
  type?: string;
  placeholder?: string;
  autoComplete?: string;
}

type PasswordFieldProps = Omit<InputFieldProps, 'type'>;

export const InputField: React.FC<InputFieldProps> = (props) => {
  const { name, placeholder } = props;
  const [field, meta] = useField(props);

  return (
    <>
      <Styled.Field>
        <Styled.Input id={name} autoComplete='off' {...field} {...props} />
        <Styled.Label htmlFor={name}>{placeholder}</Styled.Label>
      </Styled.Field>
      <ErrorMessage meta={meta} />
    </>
  );
};

export const PasswordField: React.FC<PasswordFieldProps> = (props) => {
  const { name, placeholder } = props;
  const [isPasswordVisible, setPasswordVisibility] = useState(false);
  const [field, meta] = useField(props);

  const togglePasswordVisibility = () =>
    setPasswordVisibility(!isPasswordVisible);

  return (
    <>
      <Styled.Field>
        <Styled.Input
          id={name}
          type={isPasswordVisible ? 'text' : 'password'}
          autoComplete='off'
          {...field}
          {...props}
        />
        <Styled.Label htmlFor={name}>{placeholder}</Styled.Label>
        <Styled.EyeIconWrapper
          className={classNames({ hidden: !isPasswordVisible })}
          onClick={togglePasswordVisibility}
        >
          <Styled.EyeIcon />
        </Styled.EyeIconWrapper>
      </Styled.Field>
      <ErrorMessage meta={meta} />
    </>
  );
};
