import styled from 'styled-components';

import { ReactComponent as Eye } from 'assets/icons/eye.svg';
import { getColor } from 'styles/colors';

export const Field = styled.div`
  position: relative;
  display: flex;
  
  &:not(:first-child) {
    margin-top: 20px;
  }
`;

export const Input = styled.input`
  min-width: 265px;
  height: 55px;
  box-sizing: border-box;
  flex-grow: 1;
  padding: 15px 20px 0;
  border: none;
  background-color: ${getColor('gray-light')};
  color: ${getColor('black')};
  
  &::placeholder {
    color: ${getColor('gray-dark')};
    opacity: 0;
  }

  &:focus {
    border-bottom: 2px solid ${getColor('black')};
    outline: none;
    
    &::placeholder {
      opacity: 1;
      transition: 0s .3s;
    }
  }
`;

export const Label = styled.label`
  position: absolute;
  top: 50%;
  left: 20px;
  font-size: 20px;
  transform: translate3d(0, -50%, 0);
  transition: .3s;
  
  ${Input}:focus + &,
  ${Input}:not([value='']) + & {
    top: 10px;
    font-size: 12px;
    transform: none;
  }
`;

export const EyeIconWrapper = styled.div`
  position: absolute;
  top: 50%;
  right: 15px;
  height: 100%;
  max-height: 25px;
  transform: translateY(-50%);

  &.hidden::before {
    position: absolute;
    top: 50%;
    display: block;
    width: 100%;
    height: 4px;
    background-color: ${getColor('black')};
    border-radius: 100%;
    content: '';
    transform: translateY(-50%) rotate(45deg);
  }
`;

export const EyeIcon = styled(Eye)`
  height: 100%;
`;
