import React, { DragEvent, useCallback, useState } from 'react';

import * as Styled from './styles';

interface DragAndDropProps {
  onFilesDrop: (fileList: FileList) => void;
}

export const DragAndDrop: React.FC<DragAndDropProps> = ({ children, onFilesDrop }) => {
  const [isDraggedOver, setDragOver] = useState(false);

  const handleDragOver = useCallback((event: DragEvent): void => {
    event.preventDefault();
  }, []);

  const handleDragEnter = useCallback((event: DragEvent): void => {
    event.preventDefault();

    if (event.dataTransfer.items && event.dataTransfer.items.length) {
      setDragOver(true);
    }
  }, []);

  const handleDragLeave = useCallback((event: DragEvent): void => {
    event.preventDefault();

    setDragOver(false);
  }, []);

  const handleDrop = useCallback((event: DragEvent): void => {
    event.preventDefault();

    setDragOver(false);
    onFilesDrop(event.dataTransfer.files);
  }, [onFilesDrop]);

  return (
    <Styled.Container
      onDragOver={handleDragOver}
      onDragEnter={handleDragEnter}
      onDragLeave={handleDragLeave}
      onDrop={handleDrop}
      dragged={isDraggedOver}
    >
      {children}
    </Styled.Container>
  );
};