import styled from 'styled-components';
import { transparentize } from 'polished';

import { getColor } from 'styles/colors';

export interface ContainerProps {
  dragged: boolean;
}

export const Container = styled.div<ContainerProps>`
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  width: 100%;
  height: 125px;
  border: ${props => props.dragged ? `2px dashed ${getColor('gray-dark')}` : 'none' };
  font-size: 20px;
  background-color: ${transparentize(0.67, getColor('gray-dark'))};
  color: ${getColor('gray-dark')};
`;
