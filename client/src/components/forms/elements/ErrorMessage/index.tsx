import { FieldMetaProps } from 'formik';
import React from 'react';

import * as Styled from './styles';

interface ErrorMessageProps {
  meta: FieldMetaProps<string>;
  withoutTouch?: boolean;
}

export const ErrorMessage: React.FC<ErrorMessageProps> = ({ meta, withoutTouch }) =>
  (withoutTouch || meta.touched) && meta.error ? (
    <Styled.Message>{meta.error}</Styled.Message>
  ) : null;