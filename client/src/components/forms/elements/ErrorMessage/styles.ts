import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const Message = styled.div`
  padding: 5px 0 0 20px;
  color: ${getColor('red')};
  font-size: 14px;
`;