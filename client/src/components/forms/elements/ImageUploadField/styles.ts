import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const Image = styled.img`
  box-sizing: border-box;
  max-width: 100%;
  width: 100%;
  margin-top: 20px;
  border: 2px solid ${getColor('gray-dark')};
`;
