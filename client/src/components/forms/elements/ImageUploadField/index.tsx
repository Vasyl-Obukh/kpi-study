import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useField } from 'formik';

import { DragAndDrop } from 'components/forms/elements/DragAndDrop';
import { InputFileField } from 'components/forms/elements/InputFileField';
import { ErrorMessage } from 'components/forms/elements/ErrorMessage';
import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

interface ImageUploadFieldProps {
  name: string;
}

const VALID_IMAGE_TYPES = ['image/gif', 'image/jpeg', 'image/png', 'image/svg+xml'];

const findValidImageFile = (files: FileList): File | undefined => Array.from(files)
  .find((file) => VALID_IMAGE_TYPES.includes(file.type));

export const convertImageToBase64 = (file: File): Promise<string> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    // @ts-ignore
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);

    reader.readAsDataURL(file);
  });
};

export const ImageUploadField: React.FC<ImageUploadFieldProps> = props => {
  const textLabels = useSelector(selectTextLabels);
  const [field, meta, helpers] = useField(props.name);
  const { setValue, setError } = helpers;

  const handleChange = useCallback(async (files: FileList): Promise<void> => {
    const validImageFile = findValidImageFile(files);

    if (validImageFile) {
      const convertedImageFile = await convertImageToBase64(validImageFile);
      setValue(convertedImageFile);
    } else {
      setError(textLabels['error-incompatible-image-drop']);
    }
  }, [setValue, setError, textLabels]);

  return (
    <>
      <DragAndDrop onFilesDrop={handleChange}>
        {textLabels['image-upload-pick-label']}
      </DragAndDrop>
      <InputFileField
        {...props}
        accept={VALID_IMAGE_TYPES.toString()}
        onChange={handleChange}
      >
        {textLabels['image-upload-pick-label']}
      </InputFileField>
      <ErrorMessage meta={meta} withoutTouch />
      {field.value && (
        <Styled.Image
          src={field.value}
          alt={textLabels['image-upload-alt']}
        />
      )}
    </>
  );
};