import React, { ChangeEvent, useCallback, useRef } from 'react';

import { SecondaryButton } from 'components/styled/Button';

import * as Styled from './styles';

interface InputFileFieldProps {
  name: string;
  onChange: (files: FileList) => void;
  accept?: string;
}

export const InputFileField: React.FC<InputFileFieldProps> = ({
  name,
  onChange,
  accept = '',
  children,
}) => {
  const defaultFileInputRef = useRef<HTMLInputElement>(null);

  const handleInputChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files) {
      onChange(event.target.files);
    }
  }, [onChange]);

  const handleClick = useCallback(() =>
    defaultFileInputRef.current?.click(),
  [defaultFileInputRef],
  );

  return (
    <>
      <Styled.DefaultFileInput
        type='file'
        ref={defaultFileInputRef}
        name={name}
        accept={accept}
        onChange={handleInputChange}
      />
      <SecondaryButton type='button' small onClick={handleClick}>
        {children}
      </SecondaryButton>
    </>
  );
};