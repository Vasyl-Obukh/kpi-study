import React, { useMemo } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { InputField } from 'components/forms/elements/InputField';
import { Button } from 'components/styled/Button';

import * as Styled from './styles';

export interface RoomEnterFormData {
  id: string;
}

interface RoomEnterFormProps {
  onSubmit: (values: RoomEnterFormData) => void;
}

const initialValues: RoomEnterFormData = {
  id: '',
};

export const RoomEnterForm: React.FC<RoomEnterFormProps> = ({ onSubmit }) => {
  const validationSchema = useMemo(() => Yup.object({
    id: Yup.string()
      .required('room id is required'),
  }), []);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Styled.Form>
        <InputField name='id' placeholder='Room ID' />
        <Button type='submit'>Enter room</Button>
      </Styled.Form>
    </Formik>
  );
};
