import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { Form, Formik, FormikHelpers } from 'formik';

import { InputField } from 'components/forms/elements/InputField';
import { Button } from 'components/styled/Button';
import * as meetingActions from 'store/meeting';

import * as Styled from './styles';

interface ChatFormData {
  message: string;
}

const initialValues: ChatFormData = {
  message: '',
};

export const ChatForm: React.FC<{}> = () => {
  const dispatch = useDispatch();

  const handleSubmit = useCallback((values: ChatFormData, actions: FormikHelpers<ChatFormData>): void => {
    dispatch(meetingActions.sendMessage(values.message));
    actions.resetForm();
  }, [dispatch]);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
    >
      <Form>
        <InputField name='message' placeholder='Type a new message' />
        <Button type='submit'>
          Send
          <Styled.SendIcon />
        </Button>
      </Form>
    </Formik>
  );
};
