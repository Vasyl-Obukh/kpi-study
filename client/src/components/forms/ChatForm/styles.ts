import styled from 'styled-components';

import { ReactComponent as Send } from 'assets/icons/send.svg';

export const SendIcon = styled(Send)`
  height: 20px;
  margin-left: 10px;
`;
