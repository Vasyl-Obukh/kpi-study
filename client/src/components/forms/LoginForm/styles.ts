import styled from 'styled-components';
import { Form as FormikForm } from 'formik';
import { Link } from 'react-router-dom';

import { getColor } from 'styles/colors';

export const Form = styled(FormikForm)`
  flex-grow: 1;
`;

export const RestorePasswordLink = styled(Link)`
  display: block;
  padding-top: 5px;
  color: ${getColor('black')};
  font-size: 14px;
  text-align: right;
`;

export const RegisterLink = styled(Link)`
  display: block;
  padding-top: 10px;
  color: ${getColor('black')};
  text-align: center;
`;
