import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { selectTextLabels } from 'store/config/selectors';
import * as userActions from 'store/user';
import { LoginData } from 'store/user/types';
import {
  InputField,
  PasswordField,
} from 'components/forms/elements/InputField';
import { Button } from 'components/styled/Button';

import * as Styled from './styles';

const initialValues: LoginData = {
  email: '',
  password: '',
};

export const LoginForm: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);
  const dispatch = useDispatch();

  const validationSchema = Yup.object({
    email: Yup.string()
      .required(textLabels['login-form-email-required-label'])
      .email(textLabels['error-email-invalid']),
    password: Yup.string().required(
      textLabels['login-form-password-required-label']
    ),
  });

  const handleLogin = (values: LoginData): void => {
    dispatch(userActions.login(values));
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleLogin}
    >
      <Styled.Form>
        <InputField
          name="email"
          type="email"
          placeholder={textLabels['login-form-email-label']}
        />
        <PasswordField
          name="password"
          placeholder={textLabels['login-form-password-label']}
        />
        <Styled.RestorePasswordLink to="/restore-password">
          {textLabels['login-form-restore-password-link-label']}
        </Styled.RestorePasswordLink>
        <Button type="submit">{textLabels['login-form-submit-label']}</Button>
        <Styled.RegisterLink to="/register">
          {textLabels['login-form-register-link-label']}
        </Styled.RegisterLink>
      </Styled.Form>
    </Formik>
  );
};
