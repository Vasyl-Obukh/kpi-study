import styled from 'styled-components';
import { Form as FormikForm } from 'formik';
import { Link } from 'react-router-dom';

import { getColor } from 'styles/colors';

export const Form = styled(FormikForm)`
  flex-grow: 1;
`;

export const RegisterLink = styled(Link)`
  display: block;
  padding-top: 10px;
  color: ${getColor('black')};
  text-align: center;
`;
