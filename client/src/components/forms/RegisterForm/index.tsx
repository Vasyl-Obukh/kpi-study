import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { RegisterData } from 'store/user/types';
import { selectTextLabels } from 'store/config/selectors';
import * as userActions from 'store/user';
import {
  InputField,
  PasswordField,
} from 'components/forms/elements/InputField';
import { Button } from 'components/styled/Button';

import * as Styled from './styles';

type RegisterFormData = Required<RegisterData>;

const initialValues: RegisterFormData = {
  name: '',
  email: '',
  password: '',
  passwordConfirmation: '',
};

export const RegisterForm: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);
  const dispatch = useDispatch();
  const validationSchema = Yup.object({
    name: Yup.string()
      .required(textLabels['register-form-email-required-label']),
    email: Yup.string()
      .required(textLabels['register-form-email-required-label'])
      .email(textLabels['error-email-invalid']),
    password: Yup.string()
      .required(textLabels['register-form-password-required-label'])
      .min(8, textLabels['error-password-invalid-length']),
    passwordConfirmation: Yup.string()
      .required(textLabels['register-form-password-required-label'])
      .oneOf(
        [Yup.ref('password'), null],
        textLabels['error-repeat-password-mismatch']
      ),
  });

  const handleSubmit = (values: RegisterFormData): void => {
    dispatch(userActions.register(values));
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      <Styled.Form>
        <InputField
          name="name"
          placeholder={textLabels['register-form-name-label']}
        />
        <InputField
          name="email"
          type="email"
          placeholder={textLabels['register-form-email-label']}
        />
        <PasswordField
          name="password"
          placeholder={textLabels['register-form-password-label']}
        />
        <PasswordField
          name="passwordConfirmation"
          placeholder={textLabels['register-form-repeat-password-label']}
        />
        <Button type="submit">
          {textLabels['register-form-submit-label']}
        </Button>
        <Styled.RegisterLink to="/login">
          {textLabels['register-form-login-link-label']}
        </Styled.RegisterLink>
      </Styled.Form>
    </Formik>
  );
};
