import styled from 'styled-components';

import { Button } from 'components/styled/Button';

export const EnterRoomButton = styled(Button)`
  width: auto;
  margin-bottom: 30px;
`;