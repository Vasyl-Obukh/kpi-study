import React from 'react';
import { useHistory } from 'react-router-dom';

import { Popup } from 'components/ui/Popup';
import { Title  } from 'components/styled/FormPageElements';
import { RoomEnterForm, RoomEnterFormData } from 'components/forms/RoomEnterForm';

import * as Styled from './styles';

const renderEnterRoomButton = (onOpen: () => void): JSX.Element =>
  <Styled.EnterRoomButton onClick={onOpen}>Enter room</Styled.EnterRoomButton>;

export const RoomEnter: React.FC<{}> = () => {
  const history = useHistory();
  const handleSubmit = ({ id }: RoomEnterFormData) => {
    history.push(`/room/${id}`);
  };

  return (
    <Popup render={renderEnterRoomButton}>
      <Title>Enter room</Title>
      <RoomEnterForm onSubmit={handleSubmit} />
    </Popup>
  );
};
