import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: 120px;
  grid-gap: 20px 50px;
  width: 100%;
  padding: 10px 0;
`;
