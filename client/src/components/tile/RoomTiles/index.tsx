import React from 'react';
import { useSelector } from 'react-redux';

import { RoomTile } from 'components/tile/RoomTile';
import { selectRooms } from 'store/rooms/selectors';

import * as Styled from './styles';

export const RoomTiles: React.FC<{}> = () => {
  const rooms = useSelector(selectRooms);

  return (
    <Styled.Container>
      {rooms.map(room => <RoomTile key={room._id} room={room} />)}
    </Styled.Container>
  );
};
