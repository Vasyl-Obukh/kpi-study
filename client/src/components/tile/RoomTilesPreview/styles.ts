import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { getColor } from 'styles/colors';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Title = styled.h2`
  margin: 0 0 10px 0;
  padding: 0 15px 5px;
  border-bottom: 2px solid ${getColor('black')};
`;

export const RoomsLink = styled(Link)`
  align-self: flex-end;
  text-transform: uppercase;
`;
