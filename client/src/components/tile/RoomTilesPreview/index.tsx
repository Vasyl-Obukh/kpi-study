import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import * as roomActions from 'store/rooms';
import { RoomTiles } from 'components/tile/RoomTiles';
import { Button } from 'components/styled/Button';

import * as Styled from './styles';

export const RoomTilesPreview: React.FC<{}> = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(roomActions.fetchPublicRooms(4));

    return () => {
      dispatch(roomActions.setRooms([]));
    };
  }, [dispatch]);

  return (
    <Styled.Container>
      <Styled.Title>Open Rooms</Styled.Title>
      <RoomTiles />
      <Styled.RoomsLink to='/rooms'>
        <Button small underlined>See more</Button>
      </Styled.RoomsLink>
    </Styled.Container>
  );
};
