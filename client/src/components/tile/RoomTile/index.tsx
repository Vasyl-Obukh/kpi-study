import React from 'react';

import { Room } from 'store/rooms/types';
import { useSelector } from 'react-redux';

import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

interface RoomTileProps {
  room: Room;
}

export const RoomTile: React.FC<RoomTileProps> = (props) => {
  const { title, owner, image, _id: id } = props.room;
  const textLabels = useSelector(selectTextLabels);

  return (
    <Styled.Container to={`/room/${id}`}>
      <Styled.Image src={image} alt={title} />
      <Styled.Content>
        <Styled.Title>{title}</Styled.Title>
        <Styled.Owner>
          <Styled.OwnerLabel>
            {textLabels['room-tile-owner-label']}
          </Styled.OwnerLabel>
          <Styled.OwnerName>{owner.name}</Styled.OwnerName>
        </Styled.Owner>
      </Styled.Content>
    </Styled.Container>
  );
};
