import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { getColor } from 'styles/colors';
import { includeFont } from 'styles/fonts';

export const Container = styled(Link)`
  display: flex;
  border: 2px solid ${getColor('gray-dark')};
  color: ${getColor('black')};
  text-decoration: none;
`;

export const Image = styled.img`
  flex-shrink: 0;
  width: 215px;
  height: 100%;
  object-fit: cover;
`;

export const Content = styled.div`
  flex-grow: 1;
  padding: 15px;
`;

export const Title = styled.div`
  ${includeFont('Bold')};
  font-size: 20px;
  color: ${getColor('primary')};
`;

export const Owner = styled.p`
  margin: 0;
  padding: 10px 0;
  font-size: 14px;
`;

export const OwnerLabel = styled.span`
  padding-right: 10px;
  
  &::after {
    content: ':';
  }
`;

export const OwnerName = styled.span`
  ${includeFont('Italic')};
  color: ${getColor('primary')};
`;
