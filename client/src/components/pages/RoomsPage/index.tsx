import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { PageContainer } from 'components/pages/PageContainer';
import { Main } from 'components/styled/Main';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { RoomTiles } from 'components/tile/RoomTiles';
import * as roomActions from 'store/rooms';

import * as Styled from './styles';

export const RoomsPage: React.FC<{}> = () => {
  const dispatch = useDispatch();

  const fetchPublicRooms = useCallback(() => {
    dispatch(roomActions.fetchPublicRooms(8));
  }, [dispatch]);

  useEffect(() => {
    fetchPublicRooms();

    return () => {
      dispatch(roomActions.setRooms([]));
    };
  }, [fetchPublicRooms, dispatch]);

  return (
    <PageContainer>
      <Main>
        <Container>
          <Row>
            <Styled.Wrapper>
              <RoomTiles />
              <Styled.PaginationButton onClick={fetchPublicRooms}>See more</Styled.PaginationButton>
            </Styled.Wrapper>
          </Row>
        </Container>
      </Main>
    </PageContainer>
  );
};
