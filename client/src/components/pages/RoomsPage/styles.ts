import styled from 'styled-components';

import { Button } from 'components/styled/Button';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 50px 0 75px;
`;

export const PaginationButton = styled(Button)
  .attrs({ small: true, underlined: true })`
  align-self: flex-end;
  text-transform: uppercase;
`;
