import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { PageContainer } from 'components/pages/PageContainer';
import { Main } from 'components/styled/Main';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { Wrapper, Title } from 'components/styled/FormPageElements';
import { RoomForm } from 'components/forms/RoomForm';
import { selectTextLabels } from 'store/config/selectors';
import { RoomFormData } from 'store/rooms/types';
import * as roomActions from 'store/rooms';

export const RoomCreationPage: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);
  const dispatch = useDispatch();

  const handleSubmit = useCallback((values: RoomFormData): void => {
    dispatch(roomActions.createRoom(values));
  }, [dispatch]);

  return (
    <PageContainer>
      <Main>
        <Container>
          <Row>
            <Wrapper>
              <Title>{textLabels['room-creation-page-title']}</Title>
              <RoomForm
                onSubmit={handleSubmit}
                submitButtonLabel={textLabels['room-creation-form-submit-label']}
              />
            </Wrapper>
          </Row>
        </Container>
      </Main>
    </PageContainer>
  );
};
