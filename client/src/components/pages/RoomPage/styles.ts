import styled from 'styled-components';

import { Main } from 'components/styled/Main';
import { Logo } from 'components/header/Logo/styles';
import { LogoType } from 'components/header/Logo/types';
import { getColor } from 'styles/colors';

export const Container = styled(Main)`
  position: relative;
  display: flex;
  background-color: ${getColor('primary-dim')};
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-grow: 1;
  max-height: calc(100vh - 50px);
`;

export const BackgroundLogo = styled(Logo)
  .attrs({ type: LogoType.big })`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate3d(-50%, -50%, 0);
`;
