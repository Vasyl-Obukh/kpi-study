import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { PageContainer } from 'components/pages/PageContainer';
import { PeerPanel } from 'components/room/PeerPanel';
import { SharedScreen } from 'components/room/SharedScreen';
import { UserVideoPanel } from 'components/room/UserVideoPanel';
import { ControlPanel } from 'components/room/control-panel/ControlPanel';
import { SidePanel } from 'components/room/SidePanel';
import * as meetingActions from 'store/meeting';

import * as Styled from './styles';

interface RoomQueryParams {
  id: string;
}

export const RoomPage: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const params = useParams<RoomQueryParams>();

  useEffect(() => {
    dispatch(meetingActions.initMeeting(params.id));
  }, [params, dispatch]);

  useEffect(() => () => {
    dispatch(meetingActions.disconnectSocket());
  }, [dispatch]);

  return (
    <PageContainer simplified>
      <Styled.Container>
        <Styled.BackgroundLogo />
        <Styled.Content>
          <PeerPanel />
          <SharedScreen />
          <UserVideoPanel />
          <ControlPanel />
        </Styled.Content>
        <SidePanel />
      </Styled.Container>
    </PageContainer>
  );
};
