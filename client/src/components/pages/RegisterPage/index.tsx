import React from 'react';
import { useSelector } from 'react-redux';

import { PageContainer } from 'components/pages/PageContainer';
import { Main } from 'components/styled/Main';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { Wrapper, Title } from 'components/styled/FormPageElements';
import { RegisterForm } from 'components/forms/RegisterForm';
import { selectTextLabels } from 'store/config/selectors';

export const RegisterPage: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);

  return (
    <PageContainer>
      <Main>
        <Container>
          <Row>
            <Wrapper>
              <Title>{textLabels['register-page-title']}</Title>
              <RegisterForm />
            </Wrapper>
          </Row>
        </Container>
      </Main>
    </PageContainer>
  );
};
