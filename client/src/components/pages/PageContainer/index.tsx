import React from 'react';

import { Header, SimplifiedHeader } from 'components/header/Header';
import { Footer } from 'components/footer/Footer';
import { GlobalMessageList } from 'components/global-message/GlobalMessageList';

interface PageContainerProps {
  simplified?: boolean;
}

export const PageContainer: React.FC<PageContainerProps> = ({ children, simplified }) => {
  return (
    <>
      {simplified ? <SimplifiedHeader /> : <Header />}
      <GlobalMessageList />
      {children}
      {simplified ? null : <Footer />}
    </>
  );
};
