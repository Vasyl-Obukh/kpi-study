import React from 'react';
import { useSelector } from 'react-redux';

import { PageContainer } from 'components/pages/PageContainer';
import { Main } from 'components/styled/Main';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { Wrapper, Title  } from 'components/styled/FormPageElements';
import { LoginForm } from 'components/forms/LoginForm';
import { selectTextLabels } from 'store/config/selectors';

export const LoginPage: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);

  return (
    <PageContainer>
      <Main>
        <Container>
          <Row>
            <Wrapper>
              <Title>{textLabels['login-page-title']}</Title>
              <LoginForm />
            </Wrapper>
          </Row>
        </Container>
      </Main>
    </PageContainer>
  );
};
