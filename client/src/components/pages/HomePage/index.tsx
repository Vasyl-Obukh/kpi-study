import React from 'react';

import { PageContainer } from 'components/pages/PageContainer';
import { Main } from 'components/styled/Main';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { RoomEnter } from 'components/RoomEnter';
import { RoomTilesPreview } from 'components/tile/RoomTilesPreview';

import * as Styled from './styles';

export const HomePage: React.FC<{}> = () => {
  return (
    <PageContainer>
      <Main>
        <Container>
          <Row>
            <Styled.Container>
              <RoomEnter />
              <RoomTilesPreview />
            </Styled.Container>
          </Row>
        </Container>
      </Main>
    </PageContainer>
  );
};
