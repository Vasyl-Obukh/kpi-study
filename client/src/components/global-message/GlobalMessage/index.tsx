import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Message } from 'store/global/types';
import * as globalActions from 'store/global';

import * as Styled from './styles';

interface GlobalMessageProps {
  message: Message;
}

export const GlobalMessage: React.FC<GlobalMessageProps> = ({ message }) => {
  const dispatch = useDispatch();
  const lowerCasedType = message.type.toLowerCase();

  const closeMessage = useCallback(() => {
    dispatch(globalActions.deleteMessageById(message.id));
  }, [dispatch, message]);

  useEffect(() => {
    const timeoutFunction = setTimeout(closeMessage, 3000);
    return () => clearTimeout(timeoutFunction);
  }, [closeMessage]);

  return (
    <Styled.Wrapper className={lowerCasedType}>
      <Styled.Content>
        <Styled.Type>{lowerCasedType}</Styled.Type>
        <span>{message.text}</span>
      </Styled.Content>
      <Styled.CloseMessageButton onClick={closeMessage} />
    </Styled.Wrapper>
  );
};
