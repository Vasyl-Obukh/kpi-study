import React from 'react';
import { Link } from 'react-router-dom';

import * as Styled from './styles';
import { LogoType } from './types';

interface LogoProps {
  className?: string;
  type?: LogoType;
}

export const Logo: React.FC<LogoProps> = ({ className, type = LogoType.default }) => {
  return (
    <Link className={className} to="/">
      <Styled.Logo type={type} />
    </Link>
  );
};
