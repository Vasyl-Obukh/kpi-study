import styled from 'styled-components';

import { ReactComponent as Image } from 'assets/icons/logo.svg';
import { ColorName, getColor } from 'styles/colors';

import { LogoType } from './types';

export interface LogoProps {
  type?: LogoType;
}

type LogoHeight = {
  [key in LogoType]?: string;
};

type LogoMargin = {
  [key in LogoType]?: string;
};

type LogoColor = {
  [key in LogoType]: ColorName;
};

const logoHeight: LogoHeight = {
  default: '70px',
  small: '40px',
  big: '150px',
};

const logoMargin: LogoMargin = {
  default: '0',
  small: '-5px 0',
};

const logoColor: LogoColor = {
  default: 'white',
  small: 'white',
  big: 'primary',
};

export const Logo = styled(Image)<LogoProps>`
  display: block;
  height: ${props => props.type ? logoHeight[props.type] : logoHeight[LogoType.default]};
  margin: ${props => props.type ? logoMargin[props.type] : logoMargin[LogoType.default]};
  color: ${props => getColor(props.type ? logoColor[props.type] : logoColor[LogoType.default])};
`;
