export enum LogoType {
  default = 'default',
  small = 'small',
  big = 'big',
}
