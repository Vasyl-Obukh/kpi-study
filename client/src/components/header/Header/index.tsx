import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { Logo } from 'components/header/Logo';
import { LogoType } from 'components/header/Logo/types';
import { NavigationMenu } from 'components/header/NavigationMenu';
import { SignSwitcher } from 'components/header/SignSwitcher';

import * as Styled from './styles';

export function Header() {
  return (
    <Styled.Wrapper>
      <Container>
        <Row>
          <Logo />
          <NavigationMenu />
          <Switch>
            <Route path={['/register', '/login']} exact />
            <Route path="/" component={SignSwitcher} />
          </Switch>
        </Row>
      </Container>
    </Styled.Wrapper>
  );
}

export function SimplifiedHeader() {
  return (
    <Styled.Wrapper>
      <Row>
        <Logo type={LogoType.small} />
      </Row>
    </Styled.Wrapper>
  );
}
