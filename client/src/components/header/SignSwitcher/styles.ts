import styled from 'styled-components';
import { transparentize } from 'polished';

import { getColor } from 'styles/colors';
import { ReactComponent as SignIn } from 'assets/icons/signin.svg';

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  margin: -10px 0;
`;

export const Icon = styled(SignIn)`
  height: 40px;
  color: ${getColor('white')};
`;

export const Panel = styled.div`
  position: absolute;
  z-index: 1;
  top: 100%;
  right: 0;
  min-width: 175px;
  box-sizing: border-box;
  padding: 15px 20px;
  border: solid ${getColor('gray-dark')};
  border-width: 0 1px 1px;
  box-shadow: 0 2px 5px ${transparentize(0.67, getColor('primary'))};
  background-color: ${getColor('white')};
  color: ${getColor('black')};
`;
