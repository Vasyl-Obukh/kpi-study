import React from 'react';
import { useSelector } from 'react-redux';

import { Dropdown } from 'components/ui/Dropdown';
import { LoginForm } from 'components/forms/LoginForm';
import { UserMenu } from 'components/header/UserMenu';
import { UserBlock } from 'components/header/UserBlock';
import { selectUserName } from 'store/user/selectors';

import * as Styled from './styles';

export const SignSwitcher: React.FC<{}> = () => {
  const userName = useSelector(selectUserName);

  return (
    <Styled.Wrapper>
      <Dropdown render={() => (userName ? <UserBlock /> : <Styled.Icon />)}>
        <Styled.Panel>{userName ? <UserMenu /> : <LoginForm />}</Styled.Panel>
      </Dropdown>
    </Styled.Wrapper>
  );
};
