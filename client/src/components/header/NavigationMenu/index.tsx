import React from 'react';
import { useSelector } from 'react-redux';

import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

export const NavigationMenu: React.FC<{}> = () => {
  const textLabels = useSelector(selectTextLabels);

  return (
    <Styled.Navigation>
      <Styled.Link to="/rooms">{textLabels['nav-menu-rooms']}</Styled.Link>
      <Styled.Link to="/create-room">
        {textLabels['nav-menu-create-room']}
      </Styled.Link>
    </Styled.Navigation>
  );
};
