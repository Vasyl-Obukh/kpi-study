import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { getColor } from 'styles/colors';

const linkActiveClassName = 'active';
const cornerSize = 20;
const cornerWidth = 2;

export const Navigation = styled.nav`
  display: flex;
  flex-grow: 1;
  align-items: center;
  padding: 0 25px;
`;

export const Link = styled(NavLink)
  .attrs({ activeClassName: linkActiveClassName })`
  display: inline-flex;
  align-items: center;
  padding: 5px 20px;
  margin: 0 15px;
  color: ${getColor('white')};
  font-size: 24px;
  text-decoration: none;

  &:hover,
  &.${linkActiveClassName} {
    background-image: 
      linear-gradient(to right, ${getColor('white')} ${cornerSize}px, transparent ${cornerSize}px), 
      linear-gradient(to right, ${getColor('white')} ${cornerSize}px, transparent ${cornerSize}px), 
      linear-gradient(to bottom, ${getColor('white')} ${cornerSize}px, transparent ${cornerSize}px), 
      linear-gradient(to bottom, ${getColor('white')} ${cornerSize}px, transparent ${cornerSize}px);
    background-position: 
      ${-1 * cornerSize / 2}px 0, 
      ${-1 * cornerSize / 2}px 100%, 
      0 ${-1 * cornerSize / 2}px, 
      100% ${-1 * cornerSize / 2}px;
    background-repeat: repeat-x, repeat-x, repeat-y, repeat-y;
    background-size:
      100% ${cornerWidth}px,
      100% ${cornerWidth}px,
      ${cornerWidth}px 100%,
      ${cornerWidth}px 100%;
  }
`;
