import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { includeFont } from 'styles/fonts';
import { getColor } from 'styles/colors';

export const Navigation = styled.nav`
  display: flex;
  flex-direction: column;
`;

export const Link = styled(NavLink)
  .attrs({ activeClassName: 'active' })`
  padding-bottom: 20px;
  color: ${getColor('black')}; 
  font-size: 22px;
  ${includeFont('Italic')};
  text-decoration: none;
  
  &.active {
    color: ${getColor('gray-dark')};
  }
  
  &:not(.active):hover {
    color: ${getColor('primary')};
  }
`;

export const Button = styled.div`
  cursor: pointer;
  font-size: 22px;
  ${includeFont('Italic')};

  &:hover {
    color: ${getColor('primary')};
  }
`;
