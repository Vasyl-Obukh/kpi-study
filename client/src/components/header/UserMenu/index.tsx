import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as userActions from 'store/user/actions';
import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

export const UserMenu: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const textLabels = useSelector(selectTextLabels);
  const handleLogout = () => dispatch(userActions.logout());

  return (
    <Styled.Navigation>
      <Styled.Link to="/profile/rooms">
        {textLabels['user-menu-rooms-label']}
      </Styled.Link>
      <Styled.Link to="/profile/settings">
        {textLabels['user-menu-settings-label']}
      </Styled.Link>
      <Styled.Button onClick={handleLogout}>
        {textLabels['user-menu-logout-label']}
      </Styled.Button>
    </Styled.Navigation>
  );
};
