import styled from 'styled-components';

import { getColor } from 'styles/colors';
import { ReactComponent as Profile } from 'assets/icons/profile.svg';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Icon = styled(Profile)`
  height: 40px;
  color: ${getColor('white')}
`;

export const TextBlock = styled.p`
  display: flex;
  flex-direction: column;
  padding-right: 10px;
  margin: 0;
  font-size: 14px;
  text-align: right;
`;
