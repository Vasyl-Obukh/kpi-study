import React from 'react';
import { useSelector } from 'react-redux';

import { selectUserName } from 'store/user/selectors';
import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

export const UserBlock: React.FC<{}> = () => {
  const userName = useSelector(selectUserName);
  const textLabels = useSelector(selectTextLabels);

  return (
    <Styled.Wrapper>
      <Styled.TextBlock>
        <span>{textLabels['user-welcome-label']}</span>
        <span>{userName}</span>
      </Styled.TextBlock>
      <Styled.Icon />
    </Styled.Wrapper>
  );
};
