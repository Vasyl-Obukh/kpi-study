import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Chat } from 'components/room/chat/Chat';
import { UserList } from 'components/room/user-list/UserList';
import { selectOpenedSidePanelName } from 'store/ui/selectors';
import * as uiActions from 'store/ui';
import { SidePanelName } from 'store/ui/types';

import * as Styled from './styles';

const panels: [SidePanelName, { component: JSX.Element; title: string; }][] = [
  [SidePanelName.CHAT, {
    component: <Chat />,
    title: 'Chat',
  }],
  [SidePanelName.USER_LIST, {
    component: <UserList />,
    title: 'Participants',
  }],
];

export const SidePanel: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const openedSidePanelName = useSelector(selectOpenedSidePanelName);

  const closeSidePanel = useCallback(() => {
    dispatch(uiActions.closeRoomSidePanel());
  }, [dispatch]);

  useEffect(() => closeSidePanel, [closeSidePanel]);

  return (
    <Styled.Wrapper open={Boolean(openedSidePanelName)}>
      {panels.map(([panelName, { component, title }]) => (
        <Styled.Container key={title} open={panelName === openedSidePanelName}>
          <Styled.ClosePanelButton onClick={closeSidePanel} />
          <Styled.Title>{title}</Styled.Title>
          <Styled.ContentWrapper>
            {component}
          </Styled.ContentWrapper>
        </Styled.Container>
      ))}
    </Styled.Wrapper>
  );
};
