import styled from 'styled-components';
import { transparentize } from 'polished';

import { CloseButton } from 'components/styled/CloseButton';
import { getColor } from 'styles/colors';

export interface ContainerProps {
  open?: boolean;
}

const SIDE_PANEL_WIDTH = '350px';

export const Wrapper = styled.div<ContainerProps>`
  box-sizing: border-box;
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  max-width: ${SIDE_PANEL_WIDTH};
  margin: ${props => props.open ? '0 0 0 5px' : `0 -${SIDE_PANEL_WIDTH} 0 0`};
  padding: 10px 10px 20px;
  transition: all .5s;
`;

export const Container = styled.div<ContainerProps>`
  position: absolute;
  top: 0;
  left: ${props => props.open ? 0 : SIDE_PANEL_WIDTH};
  z-index: ${props => props.open ? 0 : 1};
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 10px 10px 20px;
  background-color: ${getColor('primary-dark')};
  box-shadow: -2px 0 5px ${transparentize(0.80, getColor('black'))};
  transition: left .5s;
`;

export const ClosePanelButton = styled(CloseButton)`
  top: 10px;
  left: 10px;

  &::before,
  &::after {
    border-color: ${getColor('white')};
  }
`;

export const Title = styled.h2`
  margin: 0;
  padding: 0 0 20px 50px;
  line-height: 20px;
  color: ${getColor('white')};
`;

export const ContentWrapper = styled.div`
  flex-grow: 1;
`;
