import styled from 'styled-components';

import { Video } from 'components/videoplayer/video';

export const SharedScreenVideo = styled(Video)`
  box-sizing: border-box;
  margin-top: 20px;
  max-width: 100%;
  position: relative;
  max-height: calc(100% - 153px);
`;
