import React from 'react';
import { useSelector } from 'react-redux';

import * as StreamService from 'store/meeting/utils/streamService';
import { selectCurrentPeer, selectCurrentPeerStream, selectPeers } from 'store/meeting/selectors';
import { Peer, PopulatedPeer } from 'store/meeting/types';

import * as Styled from './styles';

export const SharedScreen: React.FC<{}> = () => {
  const currentPeerStream: MediaStream = useSelector(selectCurrentPeerStream);
  const currentPeer: Peer = useSelector(selectCurrentPeer);
  const test: Peer[] = useSelector(selectPeers);
  const populatedPeers = test.map(peer => ({
    ...peer,
    stream: StreamService.getStreamById(peer.streamId || ''),
  }));
  const peers: PopulatedPeer[] = [{ ...currentPeer, stream: currentPeerStream }, ...populatedPeers];
  const peerWithSharedScreen: PopulatedPeer | undefined = peers.find(peer => {
    if (!peer.stream) return false;
    const videoTracks: MediaStreamTrack[] = peer.stream.getVideoTracks();
    // @ts-ignore
    const videoTrack: MediaStreamTrack | undefined = videoTracks[1];
    if (!videoTrack) return false;
    return true;
  });
  if (!peerWithSharedScreen) return null;
  const videoTracks: MediaStreamTrack[] = peerWithSharedScreen.stream.getVideoTracks();
  // @ts-ignore
  const videoTrack: MediaStreamTrack | undefined = videoTracks[1];
  if (!videoTrack) return null;
  const stream: MediaStream = new MediaStream();
  stream.addTrack(videoTrack);
  return <Styled.SharedScreenVideo stream={stream} />;
};
