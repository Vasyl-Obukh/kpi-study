import styled from 'styled-components';

import { getColor } from 'styles/colors';
import { includeFont } from 'styles/fonts';

export interface ContainerProps {
  isCurrentUserMessageOwner?: boolean;
}

export const Container = styled.div<ContainerProps>`
  box-sizing: border-box;
  padding: 10px;
  margin: ${props => props.isCurrentUserMessageOwner ? '0 0 10px auto' : '0 0 10px'};
  width: 100%;
  max-width: 250px;
  font-size: 12px;
  background-color: ${props => getColor(props.isCurrentUserMessageOwner ? 'primary' : 'primary-dim')};
  color: ${getColor('white')};
`;

export const Header = styled.div`
  padding-bottom: 3px;
`;

export const Author = styled.span`
  ${includeFont('Bold')};
  padding-right: 10px;
`;

export const CreationDate = styled.span`
  color: ${getColor('gray-light')};
`;
