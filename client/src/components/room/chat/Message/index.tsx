import React from 'react';
import moment from 'moment';

import { DecoratedMessage } from 'store/meeting/types';

import * as Styled from './styles';

interface MessageProps {
  message: DecoratedMessage;
}

const formatDate = (date: string): string => moment(date).format('D/MM h:mm A');

export const Message: React.FC<MessageProps> = ({
  message: { author, date, text, isCurrentUserMessageOwner },
}) => {
  return (
    <Styled.Container isCurrentUserMessageOwner={isCurrentUserMessageOwner}>
      <Styled.Header>
        {isCurrentUserMessageOwner ? null : (
          <Styled.Author>{author.name}</Styled.Author>
        )}
        <Styled.CreationDate>{formatDate(date)}</Styled.CreationDate>
      </Styled.Header>
      <div>{text}</div>
    </Styled.Container>
  );
};
