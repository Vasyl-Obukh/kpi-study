import React from 'react';
import { useSelector } from 'react-redux';

import { ChatForm } from 'components/forms/ChatForm';
import { Message } from 'components/room/chat/Message';
import { selectDecoratedMessages } from 'store/meeting/selectors';
import { DecoratedMessage } from 'store/meeting/types';

import * as Styled from './styles';

export const Chat: React.FC<{}> = () => {
  const messages: DecoratedMessage[] = useSelector(selectDecoratedMessages);

  return (
    <Styled.Container>
      <Styled.ContentBlock>
        {messages.map(message => <Message key={message.id} message={message} />)}
      </Styled.ContentBlock>
      <ChatForm />
    </Styled.Container>
  );
};
