import styled from 'styled-components';

import { Button } from 'components/styled/Button';
import { getColor } from 'styles/colors';

export interface IconProps {
  disabled?: boolean;
}

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0;
  font-size: 18px;
  color: ${getColor('white')};
  
  &:not(:last-child) {
    border-bottom: 1px solid ${getColor('white')};
  }
`;

export const Controls = styled.div`
  display: flex;
  align-items: center;
`;

export const IconWrapper = styled.div<IconProps>`
  position: relative;
  margin-right: 5px;
  color: ${props => getColor(props.disabled ? 'red-dark' : 'white')};

  &::after {
    position: absolute;
    top: 50%;
    left: 50%;
    display: ${props => props.disabled ? 'block' : 'none'};
    width: 25px;
    height: 2px;
    background-color: ${getColor('red-dark')};
    content: '';
    transform: translate3d(-50%, -50%, 0) rotate(45deg);
  }
`;

export const Icon = styled.svg`
  height: 15px;
`;

export const ActionButton = styled(Button)
  .attrs({ small: true })`
  min-width: auto;
  margin: 0;
  
  &:not(:first-child) {
    margin-left: 5px;
  }
`;

export const ExpelButton = styled(ActionButton)`
  background-color: ${getColor('red-dark')};
`;
