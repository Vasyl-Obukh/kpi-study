import React from 'react';

import { ReactComponent as Video } from 'assets/icons/video.svg';
import { ReactComponent as Microphone } from 'assets/icons/microphone.svg';
import { Peer } from 'store/meeting/types';

import * as Styled from './styles';

interface UserProps {
  user: Peer;
}

export const User: React.FC<UserProps> = ({ user: { name, isMuted, isCameraOn } }) => {
  return (
    <Styled.Container>
      <span>{name}</span>
      <Styled.Controls>
        <Styled.IconWrapper disabled={isMuted}>
          <Styled.Icon as={Microphone} />
        </Styled.IconWrapper>
        <Styled.IconWrapper disabled={!isCameraOn}>
          <Styled.Icon as={Video} />
        </Styled.IconWrapper>
        {/* <Styled.ActionButton>Mute</Styled.ActionButton> */}
        {/* <Styled.ExpelButton>Expel</Styled.ExpelButton> */}
      </Styled.Controls>
    </Styled.Container>
  );
};
