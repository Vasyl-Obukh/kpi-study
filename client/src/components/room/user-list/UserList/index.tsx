import React from 'react';
import { useSelector } from 'react-redux';

import { User } from 'components/room/user-list/User';
import { selectPeers } from 'store/meeting/selectors';
import { Peer } from 'store/meeting/types';


export const UserList: React.FC<{}> = () => {
  const userList: Peer[] = useSelector(selectPeers);
  return (
    <>
      {userList.map(user => <User key={user.peerId} user={user} />)}
    </>
  );
};
