import styled from 'styled-components';
import { transparentize } from 'polished';

import { Video } from 'components/videoplayer/video';
import { getColor } from 'styles/colors';

export const UserVideo = styled(Video)
  .attrs({ muted: true })`
  position: fixed;
  bottom: 10px;
  left: 15px;
  height: 160px;
  width: 285px;
  object-fit: cover;
  background-color: ${getColor('black')};
  box-shadow: 0 2px 5px ${transparentize(0.67, getColor('black'))};
`;