import React from 'react';
import { useSelector } from 'react-redux';

import { selectCurrentPeerCameraState, selectCurrentPeerStream } from 'store/meeting/selectors';

import * as Styled from './styles';

export const UserVideoPanel: React.FC<{}> = () => {
  const currentUserStream: MediaStream = useSelector(selectCurrentPeerStream);
  const currentUserCameraState: boolean = useSelector(selectCurrentPeerCameraState);

  return currentUserCameraState ? <Styled.UserVideo stream={currentUserStream} /> : null;
};
