import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';

import { Icon } from 'components/room/control-panel/styles';
import { ReactComponent as HangUp } from 'assets/icons/hang-up.svg';

import * as Styled from './styles';

export const DeclineButton: React.FC<{}> = () => {
  const history = useHistory();
  const endCall = useCallback(() => {
    history.push('/');
  }, [history]);

  return (
    <Styled.DeclineIconWrapper onClick={endCall}>
      <Icon as={HangUp} />
    </Styled.DeclineIconWrapper>
  );
};
