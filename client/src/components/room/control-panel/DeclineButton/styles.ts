import styled from 'styled-components';

import { getColor } from 'styles/colors';

import { IconWrapper } from '../styles';

export const DeclineIconWrapper = styled(IconWrapper)`
  width: 100px;
  background-color: ${getColor('red-dark')};
`;
