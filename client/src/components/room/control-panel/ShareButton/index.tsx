import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

import { ReactComponent as ShareScreen } from 'assets/icons/share-screen.svg';
import { Icon, IconWrapper, Title } from 'components/room/control-panel/styles';
import * as meetingActions from 'store/meeting';
import { selectCurrentPeerSharingState } from 'store/meeting/selectors';

export const ShareButton: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const isCurrentUserShareScreen = useSelector(selectCurrentPeerSharingState);
  const title = isCurrentUserShareScreen ? 'Stop sharing' : 'Share screen';

  const toggleScreenShare = useCallback(() => {
    dispatch(meetingActions.toggleScreenShare());
  }, [dispatch]);

  return (
    <IconWrapper
      className={classNames({ disabled: isCurrentUserShareScreen })}
      onClick={toggleScreenShare}
    >
      <Icon as={ShareScreen} />
      <Title>{title}</Title>
    </IconWrapper>
  );
};
