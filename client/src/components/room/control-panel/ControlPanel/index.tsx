import React  from 'react';

import { CameraButton } from 'components/room/control-panel/CameraButton';
import { AudioButton } from 'components/room/control-panel/AudioButton';
import { ShareButton } from 'components/room/control-panel/ShareButton';
import { ChatButton } from 'components/room/control-panel/ChatButton';
import { UserListButton } from 'components/room/control-panel/UserListButton';
import { DeclineButton } from 'components/room/control-panel/DeclineButton';

import * as Styled from './styles';

export const ControlPanel: React.FC<{}> = () => {
  return (
    <Styled.Container>
      <CameraButton />
      <AudioButton />
      <ShareButton />
      <ChatButton />
      <UserListButton />
      <DeclineButton />
    </Styled.Container>
  );
};
