import styled from 'styled-components';

import { getColor } from 'styles/colors';
import { includeFont } from 'styles/fonts';

export const Container = styled.div`
  position: absolute;
  bottom: 10px;
  left: 50%;
  display: flex;
  height: 70px;
  transform: translateX(-50%);
  background-color: ${getColor('primary')};
  color: ${getColor('white')};
  box-shadow: 0 2px 5px ${getColor('black')};
`;

export const Time = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100px;
  ${includeFont('Bold')};
`;
