import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

import { ReactComponent as Video } from 'assets/icons/video.svg';
import { Icon, IconWrapper, Title } from 'components/room/control-panel/styles';
import * as meetingActions from 'store/meeting';
import { selectCurrentPeerCameraState } from 'store/meeting/selectors';

export const CameraButton: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const isCameraOn = useSelector(selectCurrentPeerCameraState);
  const title = isCameraOn ? 'Turn camera off' : 'Turn camera on';

  const toggleAudio = useCallback(() => {
    dispatch(meetingActions.toggleCamera());
  }, [dispatch]);

  return (
    <IconWrapper
      className={classNames({ disabled: !isCameraOn })}
      onClick={toggleAudio}
    >
      <Icon as={Video} />
      <Title>{title}</Title>
    </IconWrapper>
  );
};
