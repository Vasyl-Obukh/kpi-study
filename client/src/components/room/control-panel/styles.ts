import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const IconWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 90px;
  cursor: pointer;

  &.disabled::before {
    position: absolute;
    top: 50%;
    display: block;
    width: 60px;
    height: 4px;
    background-color: ${getColor('white')};
    content: '';
    transform: translateY(-50%) rotate(45deg);
  }
`;

export const Icon = styled.svg`
  height: 30px;
`;

export const Title = styled.div`
  position: absolute;
  bottom: calc(100% + 5px);
  display: none;
  padding: 5px 12px;
  font-size: 12px;
  white-space: nowrap;
  background-color: ${getColor('primary')};
  box-shadow: 0 2px 5px ${getColor('black')};
  
  *:hover > & {
    display: block;
  }
  
  &::after {
    position: absolute;
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
    width: 0;
    height: 0;
    border-top: 5px solid ${getColor('primary')};
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    content: '';
  }
`;