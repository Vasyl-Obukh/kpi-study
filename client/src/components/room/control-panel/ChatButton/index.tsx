import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ReactComponent as Chat } from 'assets/icons/chat.svg';
import { Icon, IconWrapper, Title } from 'components/room/control-panel/styles';
import * as uiActions from 'store/ui';
import { selectOpenedSidePanelName } from 'store/ui/selectors';
import { SidePanelName } from 'store/ui/types';

export const ChatButton: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const openedSidePanelName = useSelector(selectOpenedSidePanelName);
  const title = openedSidePanelName === SidePanelName.CHAT
    ? 'Close chat'
    : 'Open chat';

  const toggleChatPanel = useCallback(() => {
    if (openedSidePanelName === SidePanelName.CHAT) {
      dispatch(uiActions.closeRoomSidePanel());
    } else {
      dispatch(uiActions.setRoomOpenSidePanel(SidePanelName.CHAT));
    }
  }, [dispatch, openedSidePanelName]);

  return (
    <IconWrapper onClick={toggleChatPanel}>
      <Icon as={Chat} />
      <Title>{title}</Title>
    </IconWrapper>
  );
};
