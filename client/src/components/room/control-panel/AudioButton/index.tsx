import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

import { ReactComponent as Microphone } from 'assets/icons/microphone.svg';
import { Icon, IconWrapper, Title } from 'components/room/control-panel/styles';
import * as meetingActions from 'store/meeting';
import { selectCurrentPeerMutedState } from 'store/meeting/selectors';

export const AudioButton: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const isMuted = useSelector(selectCurrentPeerMutedState);
  const title = isMuted ? 'Unmute' : 'Mute';

  const toggleAudio = useCallback(() => {
    dispatch(meetingActions.toggleAudio());
  }, [dispatch]);

  return (
    <IconWrapper
      className={classNames({ disabled: isMuted })}
      onClick={toggleAudio}
    >
      <Icon as={Microphone} />
      <Title>{title}</Title>
    </IconWrapper>
  );
};
