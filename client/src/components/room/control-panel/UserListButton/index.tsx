import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Icon, IconWrapper, Title } from 'components/room/control-panel/styles';
import { ReactComponent as UserList } from 'assets/icons/user-list.svg';
import * as uiActions from 'store/ui';
import { selectOpenedSidePanelName } from 'store/ui/selectors';
import { SidePanelName } from 'store/ui/types';

export const UserListButton: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const openedSidePanelName = useSelector(selectOpenedSidePanelName);
  const title = openedSidePanelName === SidePanelName.USER_LIST
    ? 'Close user list'
    : 'Open user list';

  const toggleUserListPanel = useCallback(() => {
    if (openedSidePanelName === SidePanelName.USER_LIST) {
      dispatch(uiActions.closeRoomSidePanel());
    } else {
      dispatch(uiActions.setRoomOpenSidePanel(SidePanelName.USER_LIST));
    }
  }, [dispatch, openedSidePanelName]);

  return (
    <IconWrapper onClick={toggleUserListPanel}>
      <Icon as={UserList} />
      <Title>{title}</Title>
    </IconWrapper>
  );
};
