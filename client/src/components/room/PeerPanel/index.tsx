import React from 'react';
import { useSelector } from 'react-redux';

import { selectPopulatedPeers } from 'store/meeting/selectors';
import { PopulatedPeer } from 'store/meeting/types';

import * as Styled from './styles';

export const PeerPanel: React.FC<{}> = () => {
  const peers: PopulatedPeer[] = useSelector(selectPopulatedPeers);

  return peers.length ? (
    <Styled.Container>
      {peers.map(peer => {
        if (!peer.stream) return null;
        return (
          <Styled.PeerVideo 
            key={peer.peerId} 
            stream={peer.stream} 
            disabled={!peer.isCameraOn}
          />
        );
      }
      )}
    </Styled.Container>
  ) : null;
};
