import styled from 'styled-components';
import { transparentize } from 'polished';

import { Video } from 'components/videoplayer/video';
import { getColor } from 'styles/colors';

export interface PeerVideoProps {
  disabled?: boolean;
}

export const Container = styled.div`
  align-self: stretch;
  position: relative;
  z-index: 1;
  padding: 5px 45px;
  background-color: ${getColor('primary-dark')};
  box-shadow: 0 3px 10px ${transparentize(0.67, getColor('black'))};
`;

export const PeerVideo = styled(Video)<PeerVideoProps>`
  display: ${props => props.disabled ? 'none' : 'inline-block'};
  height: 120px;
  width: 215px;
  object-fit: cover;
  background-color: ${getColor('black')};
  box-shadow: 0 2px 5px ${transparentize(0.67, getColor('black'))};
  
  &:not(:last-child) {
    margin-right: ${props => props.disabled ? 0 : '5px'};
  }
`;