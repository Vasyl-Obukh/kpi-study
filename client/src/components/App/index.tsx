import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Routing } from 'components/Routing';
import * as globalActions from 'store/global';

import * as Styled from './styles';

export function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(globalActions.initApp());
  }, [dispatch]);

  return (
    <Styled.Wrapper>
      <Routing />
    </Styled.Wrapper>
  );
}
