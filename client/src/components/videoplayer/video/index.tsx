import React, { useEffect, useRef } from 'react';

interface VideoProps {
  stream: MediaStream;
  className?: string;
  muted?: boolean;
}

export const Video: React.FC<VideoProps> = ({ stream, className, muted }) => {
  const videoRef = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    if (videoRef.current) {
      videoRef.current.srcObject = stream;
    }
  }, [stream]);

  return <video muted={muted} className={className} playsInline ref={videoRef} autoPlay />;
};
