import React, { useCallback, useRef, useState } from 'react';

import { CloseButton } from 'components/styled/CloseButton';
import { useOutsideClick } from 'hooks/use-outside-click';

import * as Styled from './styles';

interface PopupProps {
  render: (onOpen: () => void) => JSX.Element;
}

export const Popup: React.FC<PopupProps> = ({ children, render }) => {
  const node = useRef<HTMLDivElement>(null);
  const [open, setOpen] = useState(false);

  const toggleOpenState = useCallback(() => {
    setOpen(!open);
  }, [open, setOpen]);

  useOutsideClick({ node, open, setOpen });

  return (
    <>
      {render(toggleOpenState)}
      {open ? (
        <Styled.Background>
          <Styled.Container ref={node}>
            <CloseButton onClick={toggleOpenState} />
            {children}
          </Styled.Container>
        </Styled.Background>
      ) : null}
    </>
  );
};
