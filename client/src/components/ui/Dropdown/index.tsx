import React, { useState, useRef, useCallback } from 'react';
import classNames from 'classnames';

import { useOutsideClick } from 'hooks/use-outside-click';

import * as Styled from './styles';

interface DropdownProps {
  render?: () => JSX.Element;
}

export const Dropdown: React.FC<DropdownProps> = ({ children, render }) => {
  const node = useRef<HTMLDivElement>(null);
  const [open, setOpen] = useState(false);

  const toggleOpenState = useCallback(() => {
    setOpen(!open);
  }, [open, setOpen]);

  useOutsideClick({ node, open, setOpen });

  return (
    <div ref={node}>
      <div onClick={toggleOpenState}>{render && render()}</div>
      <Styled.PanelWrapper className={classNames({ closed: !open })}>
        {children}
      </Styled.PanelWrapper>
    </div>
  );
};
