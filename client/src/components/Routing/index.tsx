import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import { HomePage } from 'components/pages/HomePage';
import { RegisterPage } from 'components/pages/RegisterPage';
import { LoginPage } from 'components/pages/LoginPage';
import { RoomCreationPage } from 'components/pages/RoomCreationPage';
import { RoomsPage } from 'components/pages/RoomsPage';
import { RoomPage } from 'components/pages/RoomPage';
import { history } from 'store';


export function Routing() {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route path='/register' component={RegisterPage} />
        <Route path='/login' component={LoginPage} />
        <Route path='/create-room' component={RoomCreationPage} />
        <Route path='/rooms' component={RoomsPage} />
        <Route path='/room/:id' component={RoomPage} />
      </Switch>
    </ConnectedRouter>
  );
}
