import styled from 'styled-components';

import { includeFont } from 'styles/fonts';
import { getColor } from 'styles/colors';

export const Wrapper = styled.div`
  display: flex;
  max-width: 420px;
  flex-direction: column;
  flex-grow: 1;
  padding: 45px 0;
  margin: 0 auto;
`;

export const Title = styled.h1`
  padding-left: 15px;
  margin: 10px 0;
  color: ${getColor('primary')};
  font-size: 28px;
  ${includeFont('Bold')};
  text-transform: uppercase;
`;
