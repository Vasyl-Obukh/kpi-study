import styled from 'styled-components';

import { getColor } from 'styles/colors';

export interface ButtonProps {
  small?: boolean;
  underlined?: boolean;
}

export const Button = styled.button<ButtonProps>`
  display: flex;
  width: ${props => props.small ? 'auto' : '100%'};
  min-width: ${props => props.small ? '200px' : '300px'};
  min-height: ${props => props.small ? '30px' : '55px'};
  box-sizing: border-box;
  align-items: center;
  justify-content: center;
  border: 2px solid transparent;
  margin-top: 10px;
  background-color: ${getColor('primary')};
  color: ${getColor('white')};
  cursor: pointer;
  font-size: ${props => props.small ? '16px' : '24px'};
  text-transform: inherit;
  text-decoration: ${props => props.underlined ? 'underline': 'none'};
  transition: background-color, color .5s;
  
  &:hover {
    border-color: ${getColor('primary')};
    background-color: ${getColor('white')};
    color: ${getColor('primary')};
  }
`;

export const SecondaryButton = styled(Button)`
  background-color: ${getColor('gray-dark')};

  &:hover {
    border-color: ${getColor('gray-dark')};
    color: ${getColor('gray-dark')};
  }
`;
