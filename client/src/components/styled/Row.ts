import styled from 'styled-components';

import { ROW_GUTTER } from 'styles/grid';

export const Row = styled.div`
  display: flex;
  padding: 0 ${ROW_GUTTER};
`;
