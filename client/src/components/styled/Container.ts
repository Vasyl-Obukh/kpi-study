import styled from 'styled-components';

import { MAX_WIDTH } from 'styles/grid';

export const Container = styled.div`
  position: relative;
  max-width: ${MAX_WIDTH};
  margin: 0 auto;
`;
