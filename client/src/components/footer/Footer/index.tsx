import React from 'react';

import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { SocialLinks } from 'components/footer/SocialLinks';
import { Copyright } from 'components/footer/Copyright';

import * as Styled from './styles';

export function Footer() {
  return (
    <Styled.Wrapper>
      <Container>
        <Row>
          <Styled.Content>
            <SocialLinks />
            <Copyright />
          </Styled.Content>
        </Row>
      </Container>
    </Styled.Wrapper>
  );
}
