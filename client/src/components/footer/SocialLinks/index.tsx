import React from 'react';
import { connect } from 'react-redux';

import { selectSocialLinks } from 'store/config/selectors';
import { RootReducer } from 'store/types';

import * as Styled from './styles';
import { getStyledLinksData } from './links';
import { StyledLinkData } from './types';

interface Props {
  styledLinksData: StyledLinkData[];
}

export function StatelessSocialLinks({ styledLinksData }: Props) {
  return (
    <Styled.List>
      {styledLinksData.map((link: StyledLinkData) => (
        <Styled.Item key={link.name}>
          <Styled.Link href={link.url}>
            <link.component />
          </Styled.Link>
        </Styled.Item>
      ))}
    </Styled.List>
  );
}

const mapStateToProps = (state: RootReducer): Props => ({
  styledLinksData: getStyledLinksData(selectSocialLinks(state)),
});

export const SocialLinks = connect(mapStateToProps)(StatelessSocialLinks);
