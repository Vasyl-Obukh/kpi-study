import { ReactComponent as YouTube } from 'assets/icons/youtube.svg';
import { ReactComponent as Facebook } from 'assets/icons/facebook.svg';
import { ReactComponent as Twitter } from 'assets/icons/twitter.svg';
import { ReactComponent as Instagram } from 'assets/icons/instagram.svg';
import { ReactComponent as LinkedIn } from 'assets/icons/linkedin.svg';
import { SocialLink } from 'store/config/types';

import { getStyledLink } from './styles';
import { StyledLinkComponents, StyledLinkData } from './types';

export const linkComponents: StyledLinkComponents = Object.fromEntries(Object.entries({
  youtube: YouTube,
  facebook: Facebook,
  twitter: Twitter,
  instagram: Instagram,
  linkedIn: LinkedIn,
}).map(([name, component]) =>
  [name, getStyledLink(component)],
));

export const getStyledLinksData = (socialLinks: SocialLink[]): StyledLinkData[] =>
  socialLinks.map((linkData: SocialLink) => ({
    ...linkData,
    component: linkComponents[linkData.name],
  }));
