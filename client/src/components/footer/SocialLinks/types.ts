import { FunctionComponent } from 'react';

import { SocialLink } from 'store/config/types';

export interface StyledLinkComponents {
  [key: string]: FunctionComponent;
}

export type StyledLinkData = SocialLink & {
  component: FunctionComponent;
};
