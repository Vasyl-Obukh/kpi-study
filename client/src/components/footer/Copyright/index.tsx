import React from 'react';
import { useSelector } from 'react-redux';

import { selectTextLabels } from 'store/config/selectors';

import * as Styled from './styles';

export function Copyright() {
  const textLabels = useSelector(selectTextLabels);

  return (
    <Styled.Wrapper>
      <Styled.Paragraph>{textLabels['copyright-line1']}</Styled.Paragraph>
      <Styled.Paragraph>{textLabels['copyright-line2']}</Styled.Paragraph>
    </Styled.Wrapper>
  );
}
