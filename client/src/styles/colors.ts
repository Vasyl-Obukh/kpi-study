enum Color {
  primary = '#531987',
  'primary-dim' = '#9881ad',
  'primary-dark' = '#756484',
  error = '#dd0000',
  success = '#00dd00',
  info = '#dddd00',
  black = '#000000',
  white = '#ffffff',
  gray = '#777777',
  'gray-dark' = '#878787',
  'gray-light' = '#d7d7d7',
  red = '#ff0000',
  'red-dark' = '#bb0000',
}

export type ColorName = keyof typeof Color;

export function getColor(color: ColorName): Color {
  return Color[color];
}
