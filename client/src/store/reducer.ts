import { combineReducers } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';

import globalReducer from './global';
import configReducer from './config';
import userReducer from './user';
import roomsReducer from './rooms';
import meetingReducer from './meeting';
import uiReducer from './ui';

const createRootReducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    global: globalReducer,
    config: configReducer,
    user: userReducer,
    rooms: roomsReducer,
    meeting: meetingReducer,
    ui: uiReducer,
  });

export default createRootReducer;
