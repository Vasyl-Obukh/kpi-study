import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { ConfigData, Slice } from './types';
import * as sagaActions from './actions';

const configSlice = createSlice({
  name: 'config',
  initialState: {
    data: {},
  } as Slice,
  reducers: {
    setConfigData(state, action: PayloadAction<ConfigData>) {
      return action.payload;
    },
  }
});

export const {
  fetchConfig,
  setConfigData,
} = { ...configSlice.actions, ...sagaActions };

export default configSlice.reducer;
