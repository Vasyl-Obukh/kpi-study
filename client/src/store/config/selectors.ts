import { RootReducer } from 'store/types';

import { Slice, SocialLink, TextLabels } from './types';

export const selectSlice = (state: RootReducer): Slice => state.config || {};
export const selectTextLabels = (state: RootReducer): TextLabels => selectSlice(state).textLabels || {};
export const selectSocialLinks = (state: RootReducer): SocialLink[] => selectSlice(state).socialLinks || [];
