export interface TextLabels {
  [key: string]: string;
}

export interface SocialLink {
  name: string;
  url: string;
}

export interface ConfigData {
  textLabels?: TextLabels;
  socialLinks?: SocialLink[];
}

export type Slice = ConfigData;
