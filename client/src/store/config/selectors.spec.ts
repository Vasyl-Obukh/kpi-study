import { RootReducer } from 'store/types';

import { selectSlice, selectSocialLinks, selectTextLabels } from './selectors';

describe('config/selectors', () => {
  let state: Pick<RootReducer, 'config'>;

  beforeEach(() => {
    state = {
      config: {
        textLabels: {
          'test-label': 'test label value',
        },
        socialLinks: [{
          name: 'social-link-test-name',
          url: 'https://test.kpistudy.com',
        }],
      },
    };
  });

  describe('selectSlice', () => {
    test('should return config slice', () => {
      expect(selectSlice(state as RootReducer)).toEqual(state.config);
    });

    test('should return an empty object if config is missed', () => {
      state = {} as RootReducer;

      expect(selectSlice(state as RootReducer)).toEqual({});
    });
  });

  describe('selectTextLabels', () => {
    test('should return text labels', () => {
      expect(selectTextLabels(state as RootReducer)).toEqual({
        'test-label': 'test label value',
      });
    });

    test('should return an empty object if text labels are missed', () => {
      state.config = {};

      expect(selectTextLabels(state as RootReducer)).toEqual({});
    });
  });

  describe('selectSocialLinks', () => {
    test('should return social links', () => {
      expect(selectSocialLinks(state as RootReducer)).toEqual([{
        name: 'social-link-test-name',
        url: 'https://test.kpistudy.com',
      }]);
    });

    test('should return an empty array if social links are missed', () => {
      state.config = {};

      expect(selectSocialLinks(state as RootReducer)).toEqual([]);
    });
  });
});
