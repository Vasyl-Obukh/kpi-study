import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType } from 'store/utils/actionType';

const createConfigActionType = createStoreActionType('config');

export const fetchConfig = createAction(createConfigActionType('configFetch'));
