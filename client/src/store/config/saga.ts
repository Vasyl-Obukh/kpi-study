import { call, put, takeLatest } from 'redux-saga/effects';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as sagaActions from './actions';
import * as Api from './api';
import { ConfigData } from './types';

function* tryFetchConfig() {
  const configData: ConfigData = yield call(Api.fetchData);
  yield put(actions.setConfigData(configData));
}

function* fetchConfig() {
  try {
    yield call(tryFetchConfig);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

export function* watchConfigFetch() {
  yield takeLatest(sagaActions.fetchConfig.toString(), fetchConfig);
}

export default [
  watchConfigFetch(),
];
