import { PayloadAction } from '@reduxjs/toolkit';

import reducer, { setConfigData } from '.';
import { ConfigData, Slice } from './types';

describe('config/slice', () => {
  describe('reducers', () => {
    let state: Slice;

    beforeEach(() => {
      state = {
        textLabels: {},
      };
    });

    describe('setConfigData', () => {
      test('should update config state with received data', () => {
        const action: PayloadAction<ConfigData> = {
          type: setConfigData.toString(),
          payload: {
            textLabels: {
              'test-label': 'test label value',
            },
          },
        };

        expect(reducer(state, action)).toEqual({
          textLabels: {
            'test-label': 'test label value',
          },
        });
      });
    });
  });

  describe('actions', () => {
    describe('setConfigData', () => {
      test('should create action to set config data', () => {
        const configData: ConfigData = {
          textLabels: {},
        };

        expect(setConfigData(configData).payload).toEqual({
          textLabels: {},
        });
      });
    });
  });
});
