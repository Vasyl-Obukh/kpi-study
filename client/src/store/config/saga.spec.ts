import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';
import toolkit from '@reduxjs/toolkit';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as Api from './api';
import { ConfigData } from './types';
import * as sagaActions from './actions';
import { watchConfigFetch } from './saga';

describe('config/sagas', () => {
  describe('configFetch', () => {
    test('should put data received from the api to the store', () => {
      const configData: ConfigData = {
        textLabels: {},
      };

      return expectSaga(watchConfigFetch)
        .provide([
          [call(Api.fetchData), configData],
        ])
        .put(actions.setConfigData(configData))
        .dispatch(sagaActions.fetchConfig())
        .run();
    });

    test('should set global error when fetch failed', () => {
      const error = new Error('test error');
      jest.spyOn(toolkit, 'nanoid').mockImplementation(() => 'test id');

      return expectSaga(watchConfigFetch)
        .provide([
          [call(Api.fetchData), throwError(error)],
        ])
        .put(globalActions.addMessage({
          type: MessageType.Error, text:
          error.message
        }))
        .dispatch(sagaActions.fetchConfig())
        .run();
    });
  });
});
