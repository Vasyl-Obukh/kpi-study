import { HttpClient } from 'utils/httpClient';

import { ConfigData } from './types';

const API_ROOT = '/api/config';

export const fetchData = async (): Promise<ConfigData> => {
  return HttpClient.get<ConfigData>(`${API_ROOT}/data`);
};
