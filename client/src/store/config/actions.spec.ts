import * as actions from './actions';

describe('config/actions', () => {
  describe('fetchConfig', () => {
    test('should create an action to fetch the app config', () => {
      expect(actions.fetchConfig()).toEqual({
        type: actions.fetchConfig.toString(),
      });
    });
  });
});
