import { all } from 'redux-saga/effects';

import globalSaga from './global/saga';
import configSaga from './config/saga';
import userSaga from './user/saga';
import roomSaga from './rooms/saga';
import meetingSaga from './meeting/saga';

export default function* saga() {
  yield all([...globalSaga, ...configSaga, ...userSaga, ...roomSaga, ...meetingSaga]);
}
