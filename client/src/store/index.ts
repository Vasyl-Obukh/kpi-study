import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';

import createRootReducer from './reducer';
import saga from './saga';

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: createRootReducer(history),
  middleware: [
    ...getDefaultMiddleware(),
    routerMiddleware(history),
    sagaMiddleware,
  ],
});

sagaMiddleware.run(saga);

export default store;
