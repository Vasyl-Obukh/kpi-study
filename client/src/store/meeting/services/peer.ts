import { Instance as PeerInstance } from 'simple-peer';

const peers: Map<string, PeerInstance> = new Map();

export const PeerService = {
  addPeer(peerId: string, peer: PeerInstance): void {
    peers.set(peerId, peer);
  },
  getPeerById(peerId: string): PeerInstance | undefined {
    return peers.get(peerId);
  },
  getPeers(): PeerInstance[] {
    return Array.from(peers.values());
  },
  deletePeerById(peerId: string): boolean {
    const peer: PeerInstance | undefined = this.getPeerById(peerId);
    peer?.destroy();

    return peers.delete(peerId);
  }
};