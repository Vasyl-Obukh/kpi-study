import { createSelector } from 'reselect';

import { selectUserId } from 'store/user/selectors';
import { RootReducer } from 'store/types';

import * as StreamService from './utils/streamService';
import { DecoratedMessage, Message, Peer, PeerSettings, PopulatedPeer, Slice } from './types';

export const selectSlice = (state: RootReducer): Slice =>
  state.meeting || {};

export const selectCurrentPeer = (state: RootReducer): PeerSettings =>
  selectSlice(state).currentPeer || {};

export const selectCurrentPeerId = (state: RootReducer): string =>
  selectCurrentPeer(state).peerId || '';

export const selectCurrentPeerStreamId = (state: RootReducer): string =>
  selectCurrentPeer(state).streamId || '';

export const selectCurrentPeerMutedState = (state: RootReducer): boolean =>
  Boolean(selectCurrentPeer(state).isMuted);

export const selectCurrentPeerCameraState = (state: RootReducer): boolean =>
  Boolean(selectCurrentPeer(state).isCameraOn);

export const selectCurrentPeerSharingState = (state: RootReducer): boolean =>
  Boolean(selectCurrentPeer(state).isSharing);

export const selectCurrentPeerStream = createSelector(
  selectCurrentPeerStreamId,
  (streamId: string): MediaStream => StreamService.getStreamById(streamId),
);

export const selectPeers = (state: RootReducer): Peer[] => selectSlice(state).peers || [];

export const selectPeerById = createSelector(
  selectPeers,
  peers => (id: string) => peers.find(({ peerId }) => peerId === id),
);

export const selectPeerWithSharedScreen = createSelector(
  selectPeers,
  peers => peers.find(({ isSharing }) => isSharing),
);

export const selectPopulatedPeers = createSelector(
  selectPeers,
  (peers): PopulatedPeer[] => peers.map(peer => ({
    ...peer,
    stream: StreamService.getStreamById(peer.streamId || ''),
  })),
);

export const selectMessages = (state: RootReducer): Message[] => selectSlice(state).messages || [];

export const selectDecoratedMessages = createSelector(
  selectMessages,
  selectUserId,
  (messages: Message[], currentUserId: string): DecoratedMessage[] =>
    messages.map(message => ({
      ...message,
      isCurrentUserMessageOwner: message.author._id === currentUserId,
    })),
);
