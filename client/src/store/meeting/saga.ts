import { PayloadAction } from '@reduxjs/toolkit';
import { EventChannel } from 'redux-saga';
import { all, call, delay, fork, put, take, takeLatest, select, spawn } from 'redux-saga/effects';
import Peer, { Instance as PeerInstance, SignalData } from 'simple-peer';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as sagaActions from './actions';
import { selectCurrentPeerSharingState, selectCurrentPeerStream, selectPeerById, selectPeers } from './selectors';
import * as Api from './api';
import { Message, Peer as PeerInterface } from './types';
import { createEventChannel, createEventListenerChannel, createSocketEventChannel } from './utils/eventChannel';
import * as StreamService from './utils/streamService';
import { PeerService } from './services/peer';

function* initRoom({ payload: roomId }: PayloadAction<string>) {
  try {
    yield call(setupMedia);

    Api.connect();
    Api.emit('enter-room', roomId);
    yield spawn(watchPeerListReceive);
    yield spawn(watchSocketSignal);
    yield spawn(watchIncomingCall);
    yield spawn(watchSocketDisconnectPeer);
    yield spawn(watchMessagesReceive);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message
    }));
  }
}

function* setupMedia() {
  const stream: MediaStream = yield navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true,
  });

  StreamService.addStream(stream);
  yield put(actions.setCurrentPeerMediaStreamId(stream.id));
  yield put(actions.setCurrentPeerMutedState(false));
  yield put(actions.setCurrentPeerCameraState(true));
}

function* watchPeerListReceive() {
  const socketEventChannel: EventChannel<PeerInterface[]> = yield call(createSocketEventChannel, 'sendPeerList');

  while (true) {
    try {
      const peerList: PeerInterface[] = yield take(socketEventChannel);
      yield put(actions.setPeers(Object.values(peerList)));

      yield call(callPeers);
    } catch (error) {
      socketEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* callPeers() {
  const peersToCall: PeerInterface[] = yield select(selectPeers);

  yield all(
    Object.values(peersToCall)
      .map(peer => fork(callPeer, peer))
  );
}

function* callPeer(peer: PeerInterface) {
  yield call(addPeer, peer.peerId, true);
  Api.emit('call', peer.peerId);
}

function* addPeer(peerId: string, isInitiator: boolean = false) {
  const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);

  PeerService.addPeer(peerId, new Peer({
    initiator: isInitiator,
    trickle: false,
    stream: currentPeerStream,
  }));

  yield spawn(watchPeerSignal, peerId);
  yield spawn(watchPeerStream, peerId);
  yield spawn(watchPeerData, peerId);
}

function* watchIncomingCall() {
  const socketEventChannel: EventChannel<string> = yield call(createSocketEventChannel, 'call');

  while (true) {
    try {
      const caller: PeerInterface = yield take(socketEventChannel);
      yield put(actions.addPeer(caller));
      yield call(addPeer, caller.peerId);
    } catch (error) {
      socketEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* watchSocketSignal() {
  const socketEventChannel: EventChannel<string> = yield call(createSocketEventChannel, 'signal');

  while (true) {
    try {
      const signalData: { callerId: string; signal: SignalData } = yield take(socketEventChannel);
      const peer: PeerInstance | undefined = PeerService.getPeerById(signalData.callerId);

      peer?.signal(signalData.signal);
    } catch (error) {
      socketEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* watchPeerSignal(peerId: string) {
  const peer: PeerInstance | undefined = PeerService.getPeerById(peerId);
  // @ts-ignore
  const peerEventChannel: EventChannel<string> = yield call(createEventChannel, peer, 'signal');

  while (true) {
    try {
      const signal: SignalData = yield take(peerEventChannel);

      Api.emit('signal', {
        signal,
        to: peerId,
      });
    } catch (error) {
      peerEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* watchPeerStream(peerId: string) {
  const peer: PeerInstance | undefined = PeerService.getPeerById(peerId);
  // @ts-ignore
  const peerEventChannel: EventChannel<string> = yield call(createEventChannel, peer, 'stream');

  while (true) {
    try {
      const stream: MediaStream = yield take(peerEventChannel);
      const audioTracks: MediaStreamTrack[] = stream.getAudioTracks();
      const videoTracks: MediaStreamTrack[] = stream.getVideoTracks();

      StreamService.addStream(stream);
      yield put(actions.setPeerMediaStreamId({ peerId, streamId: stream.id }));
      yield put(actions.setPeerMutedState({
        peerId,
        mute: audioTracks.length ? !audioTracks[0].enabled : true,
      }));
      yield put(actions.setPeerCameraState({
        peerId,
        isCameraOn: videoTracks.length ? videoTracks[0].enabled : false,
      }));
    } catch (error) {
      peerEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

enum PeerDataTypes {
  TOGGLE_AUDIO = 'TOGGLE_AUDIO',
  TOGGLE_CAMERA = 'TOGGLE_CAMERA',
  TOGGLE_SCREEN_SHARE = 'TOGGLE_SCREEN_SHARE',
}

type PeerDataHandlers = Record<PeerDataTypes, (peerId: string) => void>;

const peerDataHandlers: PeerDataHandlers = {
  *[PeerDataTypes.TOGGLE_AUDIO](peerId: string) {
    const peer: PeerInterface = yield select(state => selectPeerById(state)(peerId));
    yield put(actions.setPeerMutedState({ peerId, mute: !peer.isMuted }));
  },
  *[PeerDataTypes.TOGGLE_CAMERA](peerId: string) {
    const peer: PeerInterface = yield select(state => selectPeerById(state)(peerId));
    yield put(actions.setPeerCameraState({ peerId, isCameraOn: !peer.isCameraOn }));
  },
  *[PeerDataTypes.TOGGLE_SCREEN_SHARE](peerId: string) {
    const peer: PeerInterface = yield select(state => selectPeerById(state)(peerId));
    yield delay(500);
    yield put(actions.setPeerSharingState({ peerId, isSharing: !peer.isSharing }));
  }
};

function* watchPeerData(peerId: string) {
  const peer: PeerInstance | undefined = PeerService.getPeerById(peerId);
  // @ts-ignore
  const peerEventChannel: EventChannel<string> = yield call(createEventChannel, peer, 'data');

  while (true) {
    try {
      const peerDataBuffer: Uint8Array = yield take(peerEventChannel);
      const peerDataType: PeerDataTypes = peerDataBuffer.toString() as PeerDataTypes;

      yield call(peerDataHandlers[peerDataType], peerId);
    } catch (error) {
      peerEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* watchSocketDisconnectPeer() {
  const socketEventChannel: EventChannel<string> = yield call(createSocketEventChannel, 'disconnectPeer');

  while (true) {
    try {
      const peerId: string = yield take(socketEventChannel);
      const peer: PeerInterface = yield select(state => selectPeerById(state)(peerId));

      if (peer.streamId) {
        StreamService.deleteStreamById(peer.streamId);
      }
      PeerService.deletePeerById(peerId);
      yield put(actions.deletePeerById(peer.peerId));
    } catch (error) {
      socketEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function disconnectSocket() {
  Api.disconnect();
}

function* sendMessage({ payload: message }: PayloadAction<string>) {
  try {
    Api.emit('sendMessage', message);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

function* watchMessagesReceive() {
  const socketEventChannel: EventChannel<PeerInterface[]> = yield call(createSocketEventChannel, 'sendMessage');

  while (true) {
    try {
      const message: Message = yield take(socketEventChannel);
      yield put(actions.addMessage(message));
    } catch (error) {
      socketEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* toggleAudio() {
  const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);
  const audioTrack = currentPeerStream.getAudioTracks()[0];

  audioTrack.enabled = !audioTrack.enabled;
  yield put(actions.setCurrentPeerMutedState(!audioTrack.enabled));

  PeerService.getPeers().forEach(peer => {
    peer.send(PeerDataTypes.TOGGLE_AUDIO);
  });
}

function* toggleCamera() {
  const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);
  const videoTrack = currentPeerStream.getVideoTracks()[0];

  videoTrack.enabled = !videoTrack.enabled;
  yield put(actions.setCurrentPeerCameraState(videoTrack.enabled));

  PeerService.getPeers().forEach(peer => {
    peer.send(PeerDataTypes.TOGGLE_CAMERA);
  });
}

function* watchScreenSharingEnded(track: MediaStreamTrack) {
  const trackEventChannel: EventChannel<EventTarget> = yield call(createEventListenerChannel, track, 'ended');

  while (true) {
    try {
      yield take(trackEventChannel);
      const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);
      currentPeerStream.removeTrack(track);
      yield put(actions.setCurrentPeerSharingState(false));
      PeerService.getPeers().forEach(peer => {
        peer.removeTrack(track, currentPeerStream);
        peer.send(PeerDataTypes.TOGGLE_SCREEN_SHARE);
      });
    } catch (error) {
      trackEventChannel.close();
      yield put(globalActions.addMessage({
        type: MessageType.Error,
        text: error.message
      }));
    }
  }
}

function* shareScreen() {
  const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);
  // @ts-ignore
  const stream = yield navigator.mediaDevices.getDisplayMedia();
  const screenSharingTrack = stream.getVideoTracks()[0];
  currentPeerStream.addTrack(screenSharingTrack);
  yield put(actions.setCurrentPeerSharingState(true));
  yield spawn(watchScreenSharingEnded, screenSharingTrack);

  PeerService.getPeers().forEach(peer => {
    peer.addTrack(screenSharingTrack, currentPeerStream);
    peer.send(PeerDataTypes.TOGGLE_SCREEN_SHARE);
  });
}

function* unshareScreen() {
  const currentPeerStream: MediaStream = yield select(selectCurrentPeerStream);
  const screenSharingTrack: MediaStreamTrack = currentPeerStream.getVideoTracks()[1];
  screenSharingTrack.stop();
  screenSharingTrack.dispatchEvent(new Event('ended'));
}

function* toggleScreenShare() {
  const isCurrentPeerSharingScreen: boolean = yield select(selectCurrentPeerSharingState);
  if (isCurrentPeerSharingScreen) {
    yield call(unshareScreen);
  } else {
    yield call(shareScreen);
  }
}

export function* watchRoomInit() {
  yield takeLatest(sagaActions.initMeeting.toString(), initRoom);
}

export function* watchSocketDisconnectRequest() {
  yield takeLatest(sagaActions.disconnectSocket.toString(), disconnectSocket);
}

export function* watchMessageSend() {
  yield takeLatest(sagaActions.sendMessage.toString(), sendMessage);
}

export function* watchAudioToggle() {
  yield takeLatest(sagaActions.toggleAudio.toString(), toggleAudio);
}

export function* watchCameraToggle() {
  yield takeLatest(sagaActions.toggleCamera.toString(), toggleCamera);
}

export function* watchScreenShareToggle() {
  yield takeLatest(sagaActions.toggleScreenShare.toString(), toggleScreenShare);
}

export default [
  watchRoomInit(),
  watchSocketDisconnectRequest(),
  watchMessageSend(),
  watchAudioToggle(),
  watchCameraToggle(),
  watchScreenShareToggle(),
];
