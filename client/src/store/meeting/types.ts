import { User } from 'store/user/types';

export interface PeerSettings {
  peerId: string;
  streamId?: string;
  isMuted?: boolean;
  isCameraOn?: boolean;
  isSharing?: boolean;
}

export type Peer = Partial<User> & PeerSettings; // TODO: remove Partial

export type PopulatedPeer = Peer & {
  stream: MediaStream;
};

export interface Message {
  id: string;
  text: string;
  date: string;
  author: User;
}

export type DecoratedMessage = Message & {
  isCurrentUserMessageOwner: boolean;
};

export interface Slice {
  currentPeer: PeerSettings;
  peers: Peer[];
  messages: Message[];
}
