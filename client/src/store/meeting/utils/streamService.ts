const streams = new Map();

export const addStream = (stream: MediaStream): Map<string, MediaStream> => streams.set(stream.id, stream);
export const getStreamById = (id: string): MediaStream => streams.get(id);
export const deleteStreamById = (id: string): boolean => streams.delete(id);
