import { eventChannel, EventChannel, Unsubscribe } from 'redux-saga';
import { SimplePeer } from 'simple-peer';

import * as Api from '../api';

type EventObservable = typeof Api | SimplePeer;

export function createEventChannel(
  observable: EventObservable,
  event: string,
  unsubscribeFn: Unsubscribe = () => {},
): EventChannel<unknown> {
  return eventChannel(emit => {
    // @ts-ignore
    observable.on(event, (data: unknown) => emit(data));

    return unsubscribeFn;
  });
}

export function createSocketEventChannel(event: string, unsubscribeFn: Unsubscribe = () => {}): EventChannel<unknown> {
  return createEventChannel(Api, event, unsubscribeFn);
}

export function createEventListenerChannel(
  observable: EventTarget,
  eventName: string,
  unsubscribeFn: Unsubscribe = () => {},
): EventChannel<Event> {
  return eventChannel(emit => {
    observable.addEventListener(eventName, (event: Event) => emit(event));

    return unsubscribeFn;
  });
}
