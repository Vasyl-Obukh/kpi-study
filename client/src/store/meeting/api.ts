import io, { Socket } from 'socket.io-client';

import { getAuthorizationHeader } from 'utils/httpClient';

let socket: typeof Socket;

export const connect = () => {
  socket = io.connect('', {
    transportOptions: {
      polling: {
        extraHeaders: getAuthorizationHeader(),
      },
    },
  });
};

export const on = (event: string, func: Function): void => {
  socket.on(event, func);
};

export const emit = (event: string, data: unknown): void => {
  socket.emit(event, data);
};

export const disconnect = () => {
  socket.disconnect();
};
