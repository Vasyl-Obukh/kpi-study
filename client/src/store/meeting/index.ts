import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Message, Peer, Slice } from './types';
import * as sagaActions from './actions';

interface SetPeerMediaStreamIdPayload {
  peerId: string;
  streamId: string;
}

interface SetPeerMutedStatePayload {
  peerId: string;
  mute: boolean;
}

interface SetPeerCameraStatePayload {
  peerId: string;
  isCameraOn: boolean;
}

interface SetPeerSharingStatePayload {
  peerId: string;
  isSharing: boolean;
}

const meetingSlice = createSlice({
  name: 'meeting',
  initialState: {
    currentPeer: {},
    messages: [] as Message[],
  } as Slice,
  reducers: {
    setCurrentPeerId(state, action: PayloadAction<string>) {
      state.currentPeer.peerId = action.payload;
    },
    setCurrentPeerMediaStreamId(state, action: PayloadAction<string>) {
      state.currentPeer.streamId = action.payload;
    },
    setCurrentPeerMutedState(state, action: PayloadAction<boolean>) {
      state.currentPeer.isMuted = action.payload;
    },
    setCurrentPeerCameraState(state, action: PayloadAction<boolean>) {
      state.currentPeer.isCameraOn = action.payload;
    },
    setCurrentPeerSharingState(state, action: PayloadAction<boolean>) {
      state.currentPeer.isSharing = action.payload;
    },
    setPeers(state, action: PayloadAction<Peer[]>) {
      state.peers = action.payload;
    },
    addPeer(state, action: PayloadAction<Peer>) {
      state.peers.push(action.payload);
    },
    deletePeerById(state, action: PayloadAction<string>) {
      state.peers = state.peers.filter(({ peerId }) => peerId !== action.payload);
    },
    setPeerMediaStreamId(state, action: PayloadAction<SetPeerMediaStreamIdPayload>) {
      const peer = state.peers.find(({ peerId }) => peerId === action.payload.peerId);

      if (peer) {
        peer.streamId = action.payload.streamId;
      }
    },
    setPeerMutedState(state, action: PayloadAction<SetPeerMutedStatePayload>) {
      const peer = state.peers.find(({ peerId }) => peerId === action.payload.peerId);

      if (peer) {
        peer.isMuted = action.payload.mute;
      }
    },
    setPeerCameraState(state, action: PayloadAction<SetPeerCameraStatePayload>) {
      const peer = state.peers.find(({ peerId }) => peerId === action.payload.peerId);

      if (peer) {
        peer.isCameraOn = action.payload.isCameraOn;
      }
    },
    setPeerSharingState(state, action: PayloadAction<SetPeerSharingStatePayload>) {
      const peer = state.peers.find(({ peerId }) => peerId === action.payload.peerId);

      if (peer) {
        peer.isSharing = action.payload.isSharing;
      }
    },
    addMessage(state, action: PayloadAction<Message>) {
      state.messages.push(action.payload);
    },
  },
});

export const {
  initMeeting,
  disconnectSocket,
  setCurrentPeerId,
  setCurrentPeerMediaStreamId,
  setPeers,
  addPeer,
  deletePeerById,
  setPeerMediaStreamId,
  sendMessage,
  addMessage,
  toggleAudio,
  setCurrentPeerMutedState,
  setPeerMutedState,
  toggleCamera,
  setCurrentPeerCameraState,
  setPeerCameraState,
  toggleScreenShare,
  setCurrentPeerSharingState,
  setPeerSharingState,
} = { ...meetingSlice.actions, ...sagaActions };

export default meetingSlice.reducer;
