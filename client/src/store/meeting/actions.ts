import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';

const createMeetingActionType = createStoreActionType('meeting');

export const initMeeting = createAction(
  createMeetingActionType('meetingInit'),
  withPayloadType<string>(),
);
export const disconnectSocket = createAction(createMeetingActionType('socketDisconnect'));
export const sendMessage = createAction(
  createMeetingActionType('messageSend'),
  withPayloadType<string>(),
);
export const toggleAudio = createAction(createMeetingActionType('audioToggle'));
export const toggleCamera = createAction(createMeetingActionType('cameraToggle'));
export const toggleScreenShare = createAction(createMeetingActionType('screenShareToggle'));
