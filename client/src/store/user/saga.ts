import { call, put, takeLatest } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';
import { ACCESS_TOKEN } from 'utils/httpClient';

import * as actions from '.';
import * as Api from './api';
import * as sagaActions from './actions';
import { AccessToken, LoginData, RegisterData, User } from './types';

function* handleAuthentication(accessToken: AccessToken) {
  localStorage.setItem(ACCESS_TOKEN, accessToken.ACCESS_TOKEN);
  yield put(sagaActions.fetchUserData());

  yield put(push('/'));
}

function* tryRegister(registerData: RegisterData) {
  const accessToken: AccessToken = yield call(Api.register, registerData);

  yield call(handleAuthentication, accessToken);
}

function* register({ payload: registerData }: PayloadAction<RegisterData>) {
  try {
    yield call(tryRegister, registerData);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

export function* watchRegister() {
  yield takeLatest(sagaActions.register.toString(), register);
}

function* tryLogin(loginData: LoginData) {
  const accessToken: AccessToken = yield call(Api.login, loginData);

  yield call(handleAuthentication, accessToken);
}

function* login({ payload: loginData }: PayloadAction<LoginData>) {
  try {
    yield call(tryLogin, loginData);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

export function* watchLogin() {
  yield takeLatest(sagaActions.login.toString(), login);
}

function* logout() {
  localStorage.removeItem(ACCESS_TOKEN);
  yield put(actions.setUserData({} as User));
}

export function* watchLogout() {
  yield takeLatest(sagaActions.logout.toString(), logout);
}

function* tryFetchUserData() {
  const userData: User = yield call(Api.fetchUserData);
  yield put(actions.setUserData(userData));
}

function* fetchUserData() {
  try {
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token) {
      yield call(tryFetchUserData);
    }
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

export function* watchUserDataFetch() {
  yield takeLatest(sagaActions.fetchUserData.toString(), fetchUserData);
}

export default [
  watchRegister(),
  watchLogin(),
  watchLogout(),
  watchUserDataFetch(),
];