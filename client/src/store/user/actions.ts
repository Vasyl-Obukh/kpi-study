import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';

import { LoginData, RegisterData } from './types';

const createUserActionType = createStoreActionType('user');

export const register = createAction(
  createUserActionType('register'),
  withPayloadType<RegisterData>(),
);
export const login = createAction(
  createUserActionType('login'),
  withPayloadType<LoginData>(),
);
export const logout = createAction(createUserActionType('logout'));
export const fetchUserData = createAction(createUserActionType('userDataFetch'));
