import { HttpClient } from 'utils/httpClient';

import { AccessToken, LoginData, RegisterData, User } from './types';

const API_ROOT = '/api/user';

export const register = async (registerData: RegisterData): Promise<AccessToken> =>
  HttpClient.post<AccessToken>(
    `${API_ROOT}/auth/register`,
    registerData,
  );

export const login = async (loginData: LoginData): Promise<AccessToken> =>
  HttpClient.post<AccessToken>(
    `${API_ROOT}/auth/login`,
    loginData,
  );

export const fetchUserData = async (): Promise<User> =>
  HttpClient.get(`${API_ROOT}/profile`);
