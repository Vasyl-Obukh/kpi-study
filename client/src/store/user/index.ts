import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Slice, User } from './types';
import * as sagaActions from './actions';

const userSlice = createSlice({
  name: 'user',
  initialState: {} as Slice,
  reducers: {
    setUserData(state, action: PayloadAction<User>) {
      return action.payload;
    },
  },
});

export const {
  setUserData,
  register,
  login,
  logout,
  fetchUserData,
} = { ...userSlice.actions, ...sagaActions };

export default userSlice.reducer;
