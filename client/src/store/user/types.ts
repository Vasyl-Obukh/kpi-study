export interface User {
  _id: string;
  name: string;
  email: string;
}

export type Slice = User;

export interface RegisterData {
  name: string;
  email: string;
  password: string;
  passwordConfirmation?: string;
}

export interface LoginData {
  email: string;
  password: string;
}

export interface AccessToken {
  ACCESS_TOKEN: string;
}
