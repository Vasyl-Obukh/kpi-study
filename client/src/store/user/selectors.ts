import { RootReducer } from 'store/types';

import { Slice } from './types';

const selectSlice = (state: RootReducer): Slice => state.user || {};
export const selectUser = selectSlice;
export const selectUserName = (state: RootReducer): string => selectUser(state).name || '';
export const selectUserId = (state: RootReducer): string => selectUser(state)._id || '';
