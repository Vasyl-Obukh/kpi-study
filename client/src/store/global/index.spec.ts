import toolkit, { PayloadAction } from '@reduxjs/toolkit';

import reducer, { setMessage } from '.';
import { Message, MessageType, Slice } from './types';

describe('global/slice', () => {
  describe('reducers', () => {
    let state: Slice;

    beforeEach(() => {
      state = {
        messages: [],
      };
    });

    describe('addMessage', () => {
      it('should add new message', () => {
        const action: PayloadAction<Message> = {
          type: setMessage.toString(),
          payload: {
            id: 'test id',
            type: MessageType.Error,
            text: 'test message',
          }
        };

        expect(reducer(state, action).messages).toEqual([action.payload]);
      });
    });
  });

  describe('actions', () => {
    describe('setMessage', () => {
      it('should create action to add message', () => {
        jest.spyOn(toolkit, 'nanoid').mockImplementation(() => 'test id');

        expect(setMessage(MessageType.Success, 'test message').payload).toEqual({
          id: 'test id',
          type: MessageType.Success,
          text: 'test message',
        });
      });
    });
  });
});
