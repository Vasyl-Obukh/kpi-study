import * as actions from './actions';

describe('global/saga-actions', () => {
  describe('initApp', () => {
    test('should create an action to init the app', () => {
      expect(actions.initApp()).toEqual({
        type: actions.initApp.toString(),
      });
    });
  });
});
