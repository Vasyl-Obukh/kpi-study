import { all, put, takeLatest } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import * as configActions from 'store/config';
import * as userActions from 'store/user';

import * as actions from '.';
import { AddMessagePayloadType } from './actions';

function* initApp() {
  yield put(configActions.fetchConfig());
  yield put(userActions.fetchUserData());
}

export function* watchAppInit() {
  yield takeLatest(actions.initApp.toString(), initApp);
}

function* addMessage({ payload: { type, text } }: PayloadAction<AddMessagePayloadType>) {
  const messages: string[] = Array.isArray(text) ? text : [text];
  yield all(messages.map(message => put(actions.setMessage(type, message))));
  window.scrollTo({ top: 0, behavior: 'smooth' });
}

export function* watchMessageAdd() {
  yield takeLatest(actions.addMessage.toString(), addMessage);
}

export default [
  watchAppInit(),
  watchMessageAdd(),
];
