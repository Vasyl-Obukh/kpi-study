import { expectSaga } from 'redux-saga-test-plan';

import * as configActions from 'store/config/actions';

import * as sagaActions from './actions';
import { watchAppInit } from './saga';

describe('global/sagas', () => {
  describe('appInit', () => {
    test('should make initial setup of the application', () => {
      return expectSaga(watchAppInit)
        .put(configActions.fetchConfig())
        .dispatch(sagaActions.initApp())
        .run();
    });
  });
});
