import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';
import { MessageType } from './types';

export interface AddMessagePayloadType {
  type: MessageType;
  text: string | string[];
}

const createConfigActionType = createStoreActionType('global');

export const initApp = createAction(createConfigActionType('appInit'));
export const addMessage = createAction(
  createConfigActionType('messageAdd'),
  withPayloadType<AddMessagePayloadType>(),
);
