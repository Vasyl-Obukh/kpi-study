export enum MessageType {
  Success = 'SUCCESS',
  Error = 'ERROR',
  Info = 'Info',
}

export type Message = {
  id: string;
  type: MessageType;
  text: string;
};

export interface Slice {
  messages: Message[];
}
