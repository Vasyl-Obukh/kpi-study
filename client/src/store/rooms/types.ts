import { User } from 'store/user/types';

export interface Room {
  _id: string;
  title: string;
  image: string;
  private: boolean;
  owner: User;
}

export interface Slice {
  rooms: Room[];
}

export type RoomFormData = Omit<Room, '_id' | 'owner'>;
