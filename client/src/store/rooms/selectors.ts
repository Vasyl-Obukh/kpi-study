import { RootReducer } from 'store/types';

import { Slice, Room } from './types';

export const selectSlice = (state: RootReducer): Slice => state.rooms || {};
export const selectRooms = (state: RootReducer): Room[] => selectSlice(state).rooms || [];
export const selectRoomsLength = (state: RootReducer): number => selectRooms(state).length;
