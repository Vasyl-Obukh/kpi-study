import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';
import { RoomFormData } from './types';

const createRoomsActionType = createStoreActionType('rooms');

export const createRoom = createAction(
  createRoomsActionType('roomCreation'),
  withPayloadType<RoomFormData>(),
);
export const fetchPublicRooms = createAction(
  createRoomsActionType('publicRoomsFetch'),
  withPayloadType<number>()
);