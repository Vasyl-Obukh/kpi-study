import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Slice, Room } from './types';
import * as sagaActions from './actions';

const roomsSlice = createSlice({
  name: 'rooms',
  initialState: {} as Slice,
  reducers: {
    setRooms(state, action: PayloadAction<Room[]>) {
      state.rooms = action.payload;
    },
  },
});

export const {
  createRoom,
  fetchPublicRooms,
  setRooms,
} = { ...roomsSlice.actions, ...sagaActions };

export default roomsSlice.reducer;
