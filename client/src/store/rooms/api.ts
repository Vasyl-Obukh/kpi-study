import { HttpClient } from 'utils/httpClient';

import { RoomFormData } from './types';

const API_ROOT = '/api/room';

export const createRoom = async (roomCreationData: RoomFormData) =>
  HttpClient.post(
    `${API_ROOT}/data`,
    roomCreationData,
  );

export const fetchPublicRooms = async (amount: number) =>
  HttpClient.get(
    `${API_ROOT}/data`,
    {
      params: { amount },
    },
  );
