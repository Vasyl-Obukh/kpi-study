import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as sagaActions from './actions';
import { selectRoomsLength } from './selectors';
import * as Api from './api';
import { Room, RoomFormData } from './types';

function* tryCreateRoom(roomCreationData: RoomFormData) {
  yield call(Api.createRoom, roomCreationData);

  yield put(globalActions.addMessage({
    type: MessageType.Success,
    text: 'The room was successfully created',
  }));
  yield put(push('/'));
}

function* createRoom({ payload: roomCreationData }: PayloadAction<RoomFormData>) {
  try {
    yield call(tryCreateRoom, roomCreationData);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

function* tryFetchPublicRooms(amount: number) {
  const baseAmount = yield select(selectRoomsLength);
  const rooms: Room[] = yield call(Api.fetchPublicRooms, baseAmount + amount);
  yield put(actions.setRooms(rooms));
}

function* fetchPublicRooms({ payload: amount }: PayloadAction<number> ) {
  try {
    yield call(tryFetchPublicRooms, amount);
  } catch (error) {
    yield put(globalActions.addMessage({
      type: MessageType.Error,
      text: error.message,
    }));
  }
}

export function* watchRoomCreation() {
  yield takeLatest(sagaActions.createRoom.toString(), createRoom);
}

export function* watchPublicRoomsFetch() {
  yield takeLatest(sagaActions.fetchPublicRooms.toString(), fetchPublicRooms);
}

export default [
  watchRoomCreation(),
  watchPublicRoomsFetch(),
];
