import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { OpenedSidePanelName, Slice } from './types';

const uiSlice = createSlice({
  name: 'ui',
  initialState: {} as Slice,
  reducers: {
    setRoomOpenSidePanel(state, action: PayloadAction<OpenedSidePanelName>) {
      state.openedSidePanelName = action.payload;
    },
    closeRoomSidePanel(state) {
      state.openedSidePanelName = '';
    },
  },
});

export const {
  setRoomOpenSidePanel,
  closeRoomSidePanel,
} = uiSlice.actions;

export default uiSlice.reducer;
