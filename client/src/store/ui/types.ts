export enum SidePanelName {
  CHAT = 'CHAT',
  USER_LIST = 'USER_LIST',
}

export type OpenedSidePanelName = SidePanelName | '';

export interface Slice {
  openedSidePanelName: OpenedSidePanelName;
}
