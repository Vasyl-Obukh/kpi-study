import { RootReducer } from 'store/types';

import { OpenedSidePanelName, Slice } from './types';

const selectSlice = (state: RootReducer): Slice => state.ui || {};
export const selectOpenedSidePanelName = (state: RootReducer): OpenedSidePanelName =>
  selectSlice(state).openedSidePanelName;
