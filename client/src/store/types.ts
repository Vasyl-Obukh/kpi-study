import rootReducer from './reducer';

export type RootReducer = ReturnType<ReturnType<typeof rootReducer>>;
