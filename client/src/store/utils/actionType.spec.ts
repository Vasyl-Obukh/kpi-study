import { createStoreActionType } from './actionType';

describe('store/actionType', () => {
  describe('createStoreActionType', () => {
    it('should create action type based on store and type names', () => {
      expect(createStoreActionType('primary')('fetchData'))
        .toEqual('primary/fetchData');
      expect(createStoreActionType('secondary')('removeData'))
        .toEqual('secondary/removeData');
    });
  });
});
