import { RefObject, useCallback, useEffect } from 'react';

interface UseOutsideClickParams {
  node: RefObject<HTMLDivElement>;
  open: boolean;
  setOpen: (value: boolean) => void;
}

export const useOutsideClick = ({ node, open, setOpen }: UseOutsideClickParams) => {
  const handleClickOutside = useCallback((event: MouseEvent) => {
    if (!node.current?.contains(event.target as Node)) {
      setOpen(false);
    }
  }, [node, setOpen]);

  useEffect(() => {
    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, [open, handleClickOutside]);
};
