const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = app => {
  app.use(createProxyMiddleware('/api', {
    target: process.env.API_ROOT_URL || 'http://localhost',
    changeOrigin: true,
    ws: true,
    pathRewrite: {
      '^/api': '',
    },
  }));
  app.use(createProxyMiddleware('/socket', {
    target: process.env.WS_URL || 'http://localhost:4042',
    ws: true,
    changeOrigin: true,
  }));
};
