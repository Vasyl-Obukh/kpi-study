const express = require('express');
const proxy = require('http-proxy-middleware').createProxyMiddleware;
const path = require('path');

const app = express();
const PORT = process.env.PORT || 4040;

app.use(
    proxy('/api',{
        target: process.env.API_ROOT_URL || 'http://localhost',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
            '^/api': '',
        },
    })
);
app.use(proxy('/socket', {
    target: process.env.WS_URL || 'http://localhost:4042',
    changeOrigin: true,
    ws: true,
}));

app.use(express.static(__dirname));

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(PORT, () => console.log(`Listening on :${PORT}`));
